#!/usr/bin/python

# imports I understand why they are here
from zeep import Client
from zeep.transports import Transport
import http.cookiejar
import datetime
from datetime import datetime
import sys 
import subprocess 
import configparser
import os
import re # parse regular expression 
import base64
import logging
from APMSdatabase import APMSdatabase 
from setAPMSenv import SetApmsEnv
import lxml


def setLogger():
    global myLogger
    logFormat="%(funcName)-12s %(asctime)-24s %(process)d %(levelname)-6s %(message)s"
    logging.basicConfig(format=logFormat,level=logging.DEBUG)
    myLogger=logging.getLogger()

def fctSortDict(value):
    return value['Id']

def main():
#Set the environment (configuration files are in the folder: "config")
  setEnv=SetApmsEnv.SetApmsEnv()
  setLogger()
  
  envDict=setEnv.setup_environment_dict

  DATABASE=envDict.get("DATABASE")
  PROJECT=envDict.get("PROJECT")
  PROJECT_ID=int(envDict.get("PROJECT_ID"))
  USERID=int(envDict.get("USERID"))
  WSDL=envDict.get("WSDL")
  LOCATION=envDict.get("LOCATION")
  
  APMStest=APMSdatabase.APMSdatabase(PROJECT,PROJECT_ID,USERID,WSDL)
  print("ComponentTypeList :->")
#API Querying
  res=APMStest.list_of_component_type()
  print(res) 
  
  #We will filter res informations
  listTrier=[]
  for activity in res:
    #Res is a list of dictionnary
    dico={}
    lId=activity["ID"]
    leNom=activity["Name"]
    
    #Remove spaces:
    leNom=leNom.replace(" ","")
    
    #Dico filling
    dico["Id"]=lId
    dico["Nom"]=leNom
    
    #listTrier filling
    listTrier.append(dico)
    
    #listTrier sorting
    listTrier=sorted(listTrier,key=fctSortDict) #appel la fonction de trie
  print(listTrier)  


if __name__=='__main__':
  rc=main()
  sys.exit(rc)
