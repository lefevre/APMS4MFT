from zeep import Client
from zeep.transports import Transport
import http.cookiejar
import sys 
import subprocess 
import configparser
import os
import re # parse regular expression 
import base64
import logging
from APMSdatabase import APMSdatabase 
from setAPMSenv import SetApmsEnv
import lxml
import datetime
import ast


def setLogger():
    global myLogger
    logFormat="%(funcName)-12s %(asctime)-24s %(process)d %(levelname)-6s %(message)s"
    logging.basicConfig(format=logFormat,level=logging.DEBUG)
    myLogger=logging.getLogger()


def main():
#Set the environment (configuration files are in the folder: "config")
  setEnv=SetApmsEnv.SetApmsEnv()
  envDict=setEnv.setup_environment_dict
  DATABASE=envDict.get("DATABASE")
  PROJECT=envDict.get("PROJECT")
  PROJECT_ID=int(envDict.get("PROJECT_ID"))
  USERID=int(envDict.get("USERID"))
  WSDL=envDict.get("WSDL")
  LOCATION=envDict.get("LOCATION")
  CHIP=envDict.get("ALPIDEB-CHIP")#367 pour ITSP
  TRAY=envDict.get("ALPIDEB-TRAY")#387 pour ITSP
  STATUTDICT=setEnv.set_status_dict(935)
  LOCATIONDICT=setEnv.set_location_dict(935)
  ATCTDICT=setEnv.set_atct_dict(935)
  MATERIALCONFORMITYDICT=setEnv.set_material_conformity_dict(935)
  RESULTDICT=setEnv.set_result_dict(935)
  PARAMETERSDICT=setEnv.set_parameters_dict(935)
 
  print("few debug here",PROJECT,PROJECT_ID,USERID,WSDL)
  APMStest=APMSdatabase.APMSdatabase(PROJECT,PROJECT_ID,USERID,WSDL)
  
  
  print(PARAMETERSDICT)
  print(STATUTDICT)
  print(LOCATIONDICT)
  print(ATCTDICT)
  print(MATERIALCONFORMITYDICT)
  print(RESULTDICT)
  

  sys.exit()


if __name__=='__main__':
  rc=main()
  sys.exit(rc)




























