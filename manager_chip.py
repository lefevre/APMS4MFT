"""This module is intended to contain all methods related to chip.
"""

__author__ = "Roman Lavicka"

# standard classes
import ast
import os
import sys

# local classes
from setAPMSenv import SetApmsEnv
from APMSdatabase import APMSdatabase

# global variables for this module
use_local_storage = False
local_storage_file_path = ""
environment_setup = SetApmsEnv.SetApmsEnv()
environment_dict = environment_setup.setup_environment_dict
type_id_alpideb_chip = environment_dict.get("ALPIDEB-CHIP")
db = APMSdatabase.APMSdatabase(
    environment_dict.get("PROJECT"),
    int(environment_dict.get("PROJECT_ID")),
    int(environment_dict.get("USERID")),
    environment_dict.get("WSDL"),
)


class ChipManager:
    """Manager to control chips in database.
    """

    def __init__(self, environment_setup_, use_local_storage_):
        self.environment_setup = environment_setup_
        self.environment_dict = environment_setup.setup_environment_dict
        self.type_id_alpideb_chip = environment_dict.get("ALPIDEB-CHIP")
        self.db = APMSdatabase.APMSdatabase(
            environment_dict.get("PROJECT"),
            int(environment_dict.get("PROJECT_ID")),
            int(environment_dict.get("USERID")),
            environment_dict.get("WSDL"),
        )
        self.local_storage_file_path = "localData/ChipsInMFT.txt"
        print(self.switch_data_storage(use_local_storage_))
        return

    def switch_data_storage(self, on_off):
        """Function to swithc between local storage and other.
        If you switch it on, local database is actualized.
        """
        message = "Chip Manager set to use remote database."
        global use_local_storage

        use_local_storage = on_off

        if use_local_storage:
            message = "Chip Manager set to use local database."
            self.store_locally_all_chips()

        return message

    def store_locally_all_chips(self):
        """Function to store all info on chip components from DB locally.
        """
        # Remove the file.
        try:
            os.remove(self.local_storage_file_path)
        except:
            sys.exit("Error while deleting file : " + str(self.local_storage_file_path))

        # API Querying
        list_of_components = self.db.list_component_of_type(self.type_id_alpideb_chip)

        # Save result in a file
        file = open(self.local_storage_file_path, "w")
        file.write(str(list_of_components))
        file.close()

        return list_of_components

    def create_new_chip(self, chip_component_id):
        """Function to create chip and return its ID.
        Should contain all safeguards.
        """
        print("Creating new chip " + str(chip_component_id))

        new_chip = db.new_component(self.type_id_alpideb_chip, chip_component_id, "", "", "", "", -1)
        chip_db_id = new_chip["ID"]

        if use_local_storage:
            self.store_locally_all_chips()

        # if chip already exists in database, new_chip["ID"] returns None. This should return its ID
        if chip_db_id is None:
            chip_db_id = self.get_chip_db_id_from_component_id(chip_component_id)

        return chip_db_id

    def get_chip_db_id_from_component_id(self, chip_component_id):
        """Function to return Chip ID from component ID.
        """
        print("Start looking for ID of chip " + str(chip_component_id))
        if use_local_storage:
            chip_db_id = self.get_chip_id_from_local_database(chip_component_id)
        else:
            chip_db_id = self.get_chip_db_id_from_db(chip_component_id)

        return chip_db_id

    def get_chip_db_id_from_db(self, chip_component_id):
        """Function to return Chip ID from database.
        """
        chip_db_id = -999
        chips = self.db.list_component_of_type(self.type_id_alpideb_chip)
        for chip in chips:
            component_id = chip["ComponentID"]
            if component_id == chip_component_id:
                chip_db_id = chip["ID"]

        return chip_db_id

    def get_chip_id_from_local_database(self, chip_component_id):
        """Function to return Chip ID from locally stored data.
        Need to update data from DB, but should save time.
        """
        chip_db_id = -999
        input_file = open(self.local_storage_file_path)
        chips = input_file.read()
        input_file.close()

        chips = ast.literal_eval(chips)
        for chip in chips:
            component_id = chip["ComponentID"]
            if component_id == chip_component_id:
                chip_db_id = chip["ID"]

        return chip_db_id

    @staticmethod
    def get_chip_position(chip_component_id, hic_traveler):
        """Function to return a position of a chip on a ladder.
         chip_component_id is the MFT chip ID written in HIC traveler (i.e. T716118W14R33).
         hic_traveler is a xml file related to this chip
         """
        position = "Undefined"

        # Checking first Tray
        if chip_component_id == hic_traveler['my:mesChamps']['my:ChipID_1']:
            position = "HA5"
        if chip_component_id == hic_traveler['my:mesChamps']['my:ChipID_2']:
            position = "HA4"
        if chip_component_id == hic_traveler['my:mesChamps']['my:ChipID_3']:
            position = "HA3"
        if chip_component_id == hic_traveler['my:mesChamps']['my:ChipID_4']:
            position = "HA2"
        if chip_component_id == hic_traveler['my:mesChamps']['my:ChipID_5']:
            position = "HA1"
        # Checking second Tray
        if chip_component_id == hic_traveler['my:mesChamps']['my:ChipID_HA4_b']:
            position = "HA4"
        if chip_component_id == hic_traveler['my:mesChamps']['my:ChipID_HA3_b']:
            position = "HA3"
        if chip_component_id == hic_traveler['my:mesChamps']['my:ChipID_HA2_b']:
            position = "HA2"
        if chip_component_id == hic_traveler['my:mesChamps']['my:ChipID_HA1_b']:
            position = "HA1"

        return position

    @staticmethod
    def get_n_chips_from_hic_traveler(hic_traveler):
        """Function to return quantity of chips from hic traveler
        """
        n_chips = 0

        # Checking first Tray
        if hic_traveler['my:mesChamps']['my:ChipID_1'] is not None:
            n_chips += 1
        if hic_traveler['my:mesChamps']['my:ChipID_2'] is not None:
            n_chips += 1
        if hic_traveler['my:mesChamps']['my:ChipID_3'] is not None:
            n_chips += 1
        if hic_traveler['my:mesChamps']['my:ChipID_4'] is not None:
            n_chips += 1
        if hic_traveler['my:mesChamps']['my:ChipID_5'] is not None:
            n_chips += 1
        # Checking second Tray
        if hic_traveler['my:mesChamps']['my:ChipID_HA4_b'] is not None:
            n_chips += 1
        if hic_traveler['my:mesChamps']['my:ChipID_HA3_b'] is not None:
            n_chips += 1
        if hic_traveler['my:mesChamps']['my:ChipID_HA2_b'] is not None:
            n_chips += 1
        if hic_traveler['my:mesChamps']['my:ChipID_HA1_b'] is not None:
            n_chips += 1

        return n_chips

    @staticmethod
    def get_list_of_chip_component_ids(hic_traveler):
        """Function to return a list of chip component IDs written in hic traveler
        """
        list_of_chips_ids = []

        # Checking first Tray
        if hic_traveler['my:mesChamps']['my:ChipID_1'] is not None:
            list_of_chips_ids.append(str(hic_traveler['my:mesChamps']['my:ChipID_1']))
        if hic_traveler['my:mesChamps']['my:ChipID_2'] is not None:
            list_of_chips_ids.append(str(hic_traveler['my:mesChamps']['my:ChipID_2']))
        if hic_traveler['my:mesChamps']['my:ChipID_3'] is not None:
            list_of_chips_ids.append(str(hic_traveler['my:mesChamps']['my:ChipID_3']))
        if hic_traveler['my:mesChamps']['my:ChipID_4'] is not None:
            list_of_chips_ids.append(str(hic_traveler['my:mesChamps']['my:ChipID_4']))
        if hic_traveler['my:mesChamps']['my:ChipID_5'] is not None:
            list_of_chips_ids.append(str(hic_traveler['my:mesChamps']['my:ChipID_5']))
        # Checking second Tray
        if hic_traveler['my:mesChamps']['my:ChipID_HA4_b'] is not None:
            list_of_chips_ids.append(str(hic_traveler['my:mesChamps']['my:ChipID_HA4_b']))
        if hic_traveler['my:mesChamps']['my:ChipID_HA3_b'] is not None:
            list_of_chips_ids.append(str(hic_traveler['my:mesChamps']['my:ChipID_HA3_b']))
        if hic_traveler['my:mesChamps']['my:ChipID_HA2_b'] is not None:
            list_of_chips_ids.append(str(hic_traveler['my:mesChamps']['my:ChipID_HA2_b']))
        if hic_traveler['my:mesChamps']['my:ChipID_HA1_b'] is not None:
            list_of_chips_ids.append(str(hic_traveler['my:mesChamps']['my:ChipID_HA1_b']))

        return list_of_chips_ids

    def set_all_chip_positions_on_ladder(self, ladder_db_id, hic_traveler):
        """Function to set position of chips onl adder using database.
        Large complexity is due to a format of ladder in db.
        Necessary chip db id is deep inside the structure and
        one has to peel out several layers.
        Very early, but working version.
        """
        print("Assigning positions of chips on ladder from hic traveler")
        # Refresh database of ladders
        list_ladder = []
        ladder_composition = []
        ######################
        # Workaround to get correct type
        res = self.db.show_component(ladder_db_id, "")
        list_ladder.append(res)

        # Temporary save
        file = open("temp.txt", "w")
        file.write(str(list_ladder))
        file.close()

        input_file = open("temp.txt")
        list_ladder = input_file.read()
        input_file.close()
        os.remove("temp.txt")
        #######################

        list_ladder = ast.literal_eval(list_ladder)
        for layer0 in list_ladder:
            ladder_composition = layer0["Composition"]

        composition_components = ladder_composition["ComponentComposition"]

        for component in composition_components:
            current_component = component["Component"]
            current_component_composition_id = component["ID"]
            current_component_type = current_component["ComponentType"]
            current_component_id = current_component["ComponentID"]  # i.e. T716118W14R33

            if current_component_type["ID"] == int(self.type_id_alpideb_chip):
                # extract -MFT from alpideb_chip id name
                current_chip_id = current_component_id[0:(len(current_component_id) - 4)]
                # get info for database
                chip_db_id = current_component_composition_id
                chip_position = self.get_chip_position(current_chip_id, hic_traveler)
                # API Querying
                db.component_composition_position_change(chip_db_id, chip_position, -1)

        return
