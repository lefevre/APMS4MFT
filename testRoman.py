"""This script serves purely for testing
"""

import sys

from APMSdatabase import APMSdatabase
from setAPMSenv import SetApmsEnv
import time

# local scripts
import helpers
from manager_ladder import LadderManager
from manager_fpc import FPCManager
from manager_chip import ChipManager
from manager_documents import DocumentsManager
from manager_activity import ActivityManager

# global setting
use_local_storage = False
environment_setup = SetApmsEnv.SetApmsEnv()
environment_dict = environment_setup.setup_environment_dict


def main():
    """Corpse of this testing macro
    """
    ground_logic(True)

    # Set the environment (configuration files are in the folder: "config")
    # db = APMSdatabase.APMSdatabase(
    #         environment_dict.get("PROJECT"),
    #         int(environment_dict.get("PROJECT_ID")),
    #         int(environment_dict.get("USERID")),
    #         environment_dict.get("WSDL"),
    #         )

    # Setup
    hic_number = 3050
    fpc_number = "24aA102"
    n_chips = 3
    # activity_manager = ActivityManager(environment_setup,use_local_storage)
    # ladder_manager = LadderManager(environment_setup,n_chips,use_local_storage)
    # fpc_manager = FPCManager(environment_setup,n_chips,use_local_storage)
    # manager_chip = ChipManager(environment_dict,use_local_storage)
    # hic_manager = DocumentsManager(hic_number)
    # fpc_manager = DocumentsManager(fpc_number,False)

    # Auxilliary setup
    ladder_component_name = "HIC_" + str(hic_number) + "-test"
    fpc_component_name = "FPC_" + str(fpc_number) + "-test"


    # print(hic_manager.get_location())
    # print(fpc_manager.get_location())


    # print(activity_manager.options_location("FPC-CHK-EQU-ELEC-3"))
    # print(type(activity_manager.options_location("FPC-CHK-EQU-ELEC-3")))

    # fpc_manager.set_fpc_type_id()
    # print(fpc_manager.type_id_fpc)
    # fpc_manager.n_chips = 1
    # print(fpc_manager.type_id_fpc)
    # fpc_manager.set_fpc_type_id()
    # print(fpc_manager.type_id_fpc)


    # activity_manager.perform_activity_on_component(True)

    # activity_manager.change_activity(True)

    # create_component_from_hic_traveler(db,ladder_component_name, hic_number)

    # activity_manager.standard_fpc_activity(fpc_component_name,fpc_number,"FPC-CHK-EQU-METR-3")
    #
    # activity_manager.standard_ladder_activity(ladder_component_name,hic_number,"LADDER-GLUE-3",True)
    #
    # activity_manager.standard_ladder_activity(ladder_component_name,hic_number,"LADDER-CHK-VISU-3")


    # list_ = db.listActivitiesOfType(1742)
    # list_ = db.activityTypeReadAll(1742)
    # list_ = db.activityTypeReadAll(1788)
    # print(list_)
    # for activity in list_[0]:
    #     print(activity["Name"])
    # list_ = db.listActivitiesTypes()
    # print(list_)
    # print(db.readOneActivity(96604))

    # input_file = open("config/allowed_activities.txt")
    # list_ladder = input_file.read()  #
    # input_file.close()
    #
    # print(list_ladder)
    # list_ladder = ast.literal_eval(list_ladder)
    # print(list_ladder)


    return "Script testRoman finished."

def ground_logic(is_testing_setup=False):
    """Ground logic. This contains everything.
    """
    activity_manager = ActivityManager(environment_setup,use_local_storage)

    # Set options
    options = ["Perform an activity.", "Change an activity.",
               "Create in database components from traveler.", "Nothing. Close this."]
    answers = -1

    menu_on = True
    while menu_on:
        incorrect_choice = True
        while incorrect_choice:
            print("\nWhat do you want to do (Choose a number)?")
            for i_option in range(len(options)):
                print("\n " + str(i_option) + " : " + str(options[i_option]))
            choice = input("\nEnter a number: ")

            passed_integer_control = False
            try:
                int(choice)
                passed_integer_control = True
            except ValueError:
                print("****** ERROR: Enter an integer! ******")

            if passed_integer_control:
                for i_choice in range(len(options)):
                    if int(choice) == i_choice:
                        answers = i_choice
                        incorrect_choice = False

                if incorrect_choice:
                    print("****** ERROR: Incorrect choice. Please, choose one in range 0 to "
                          + str(len(options) - 1) + "! ******")

        # Block for actions
        if answers == 0:
            activity_manager.perform_activity_on_component(is_testing_setup)
        if answers == 1:
            activity_manager.change_activity(is_testing_setup)
        if answers == 2:
            create_component_from_hic_traveler(is_testing_setup)
        if answers == 3:
            menu_on = False

    return print("\nAll tasks done!")

def create_component_from_hic_traveler(is_testing_setup):
    """Function to create ladder and its FPC and chips if necessary.
    It also attaches  components to the ladder and set chip position of chips on ladder."""
    print("\nCreating new ladder and attaching components to it.")

    # Setup
    ladder_number = int(input("\nEnter the ladder number (i.e. 3154). Pay attention to input correctly! "))

    n_chips = helpers.get_n_chips_from_ladder_number(ladder_number)

    if is_testing_setup:
        ladder_component_name = "HIC_" + str(ladder_number) + "-test"
    else:
        ladder_component_name = "HIC_" + str(ladder_number)


    manager_ladder = LadderManager(environment_dict,n_chips,use_local_storage)
    manager_fpc = FPCManager(environment_dict,n_chips,use_local_storage)
    manager_chip = ChipManager(environment_dict,use_local_storage)
    manager_traveler = DocumentsManager(ladder_number)

    # Create the new ladder
    ladder_db_id = manager_ladder.create_new_ladder(ladder_component_name)

    # Assign and create (if necessary) new FPCs
    fpc_db_id = manager_fpc.get_fpc_db_id_from_hic_traveler(manager_traveler.hic_traveler)
    if fpc_db_id == -999:
        print("FPC not found in database. Creating a new one. Source: HIC traveler.")
        fpc_db_id = manager_fpc.create_new_fpc(
            manager_fpc.get_fpc_component_name_from_hic_traveler(manager_traveler.hic_traveler))
    manager_ladder.db.place_component_in_composition(ladder_db_id, fpc_db_id, "", -1)

    # Assign and create (if necessary) new chips
    list_of_chip_component_ids = manager_chip.get_list_of_chip_component_ids(manager_traveler.hic_traveler)
    for chip in list_of_chip_component_ids:
        chip_db_id = manager_chip.get_chip_db_id_from_component_id(chip + "-MFT")
        if chip_db_id == -999:
            print("Chip not found in database. Creating a new one. Source: HIC traveler.")
            chip_db_id = manager_chip.create_new_chip(chip + "-MFT")
        manager_ladder.db.place_component_in_composition(ladder_db_id, chip_db_id, "", -1)

    # Assigning positions
    manager_chip.set_all_chip_positions_on_ladder(ladder_db_id, manager_traveler.hic_traveler)

    return



if __name__ == '__main__':
    start_time = time.time()
    rc = main()
    print("--- %s seconds ---" % (time.time() - start_time))
    sys.exit(rc)
