"""This module is intended to contain all methods related to FPC.
"""

__author__ = "Roman Lavicka"

# standard classes
import ast
import os
import sys

# local classes
from setAPMSenv import SetApmsEnv
from APMSdatabase import APMSdatabase

# global variables for this module
use_local_storage = False
local_storage_file_path = ""
environment_setup = SetApmsEnv.SetApmsEnv()
environment_dict = environment_setup.setup_environment_dict
type_id_fpc = -1
n_chips = -1
db = APMSdatabase.APMSdatabase(
        environment_dict.get("PROJECT"),
        int(environment_dict.get("PROJECT_ID")),
        int(environment_dict.get("USERID")),
        environment_dict.get("WSDL"),
        )

class FPCManager:
    """Manager to control FPCs in database.
    """

    def __init__(self, environment_setup_, n_chips_, use_local_storage_):
        self.environment_setup = environment_setup_
        self.environment_dict = environment_setup.setup_environment_dict
        self.type_id_fpc = environment_dict.get("FPC-3")
        self.db = APMSdatabase.APMSdatabase(
                environment_dict.get("PROJECT"),
                int(environment_dict.get("PROJECT_ID")),
                int(environment_dict.get("USERID")),
                environment_dict.get("WSDL"),
                )
        self.local_storage_file_path = "localData/FPCsInMFT.txt"
        print(self.switch_data_storage(use_local_storage_))
        self.type_id_fpc = -1
        self.n_chips = n_chips_
        self.set_fpc_type_id()
        return

    def set_fpc_type_id(self):
        """Set the correct fpc type id with respect to number of chips on the fpc
        """
        try:
            int(self.n_chips)
        except ValueError:
            sys.exit("\n****** ERROR: Number of chips is not an integer! ******")

        if int(self.n_chips) == 2:
            self.type_id_fpc = self.environment_dict.get("FPC-2")
            return 2
        elif int(self.n_chips) == 3:
            self.type_id_fpc = self.environment_dict.get("FPC-3")
            return 3
        elif int(self.n_chips) == 4:
            self.type_id_fpc = self.environment_dict.get("FPC-4")
            return 4
        elif int(self.n_chips) == 5:
            self.type_id_fpc = self.environment_dict.get("FPC-5")
            return 5
        else:
            sys.exit("\n****** ERROR: Invalid number of chips! ******")

    def switch_data_storage(self, on_off):
        """Function to swithc between local storage and other.
        If you switch it on, local database is actualized.
        """
        message = "FPC Manager set to use remote database."
        global use_local_storage

        use_local_storage = on_off

        if use_local_storage:
            message = "FPC Manager set to use local database."
            self.store_locally_all_fpcs()

        return message

    def store_locally_all_fpcs(self):
        """Function to store all info on fpc components from DB locally.
        """
        # Remove the file.
        try:
            os.remove(self.local_storage_file_path)
        except:
            sys.exit("Error while deleting file : " + str(self.local_storage_file_path))

        # Set type id
        self.set_fpc_type_id()

        # API Querying
        list_of_components = self.db.list_component_of_type(self.type_id_fpc)

        # Save result in a file
        file = open(self.local_storage_file_path, "w")
        file.write(str(list_of_components))
        file.close()

        return list_of_components

    def create_new_fpc(self, fpc_component_name):
        """Function to create FPC and return its ID.
        Should contain all safeguards.
        """
        print("Creating new FPC " + str(fpc_component_name))

        # Set type id
        self.set_fpc_type_id()

        new_fpc = self.db.new_component(self.type_id_fpc, fpc_component_name, "", "", "", "", -1)
        fpc_db_id = new_fpc["ID"]

        if use_local_storage:
            self.store_locally_all_fpcs()

        # if FPC already exists in database, new_fpc["ID"] returns None. This should return its ID
        if fpc_db_id is None:
            fpc_db_id = self.get_fpc_db_id_from_component_name(fpc_component_name)

        return fpc_db_id


    @staticmethod
    def get_fpc_component_name_from_hic_traveler(hic_traveler):
        """Function to return MFT FPC component name from hic traveler.
        """
        return hic_traveler['my:mesChamps']['my:FPCID']

    @staticmethod
    def get_fpc_component_name_from_fpc_form(fpc_form):
        """Function to return MFT FPC component name from fpc form.
        """
        return fpc_form['my:mesChamps']['my:FPCid']


    def get_fpc_db_id_from_hic_traveler(self, hic_traveler):
        """Function to return FPC DB ID from hic traveler.
        """
        fpc_component_id = self.get_fpc_component_name_from_hic_traveler(hic_traveler)
        if use_local_storage:
            fpc_db_id = self.get_fpc_db_id_from_local_storage(fpc_component_id)
        else:
            fpc_db_id = self.get_fpc_db_id_from_db(fpc_component_id)

        return fpc_db_id

    def get_fpc_db_id_from_fpc_form(self, fpc_form):
        """Function to return FPC DB ID from fpc form.
        """
        fpc_component_id = self.get_fpc_component_name_from_fpc_form(fpc_form)
        if use_local_storage:
            fpc_db_id = self.get_fpc_db_id_from_local_storage(fpc_component_id)
        else:
            fpc_db_id = self.get_fpc_db_id_from_db(fpc_component_id)

        return fpc_db_id



    def get_fpc_db_id_from_component_name(self, fpc_component_name):
        """Function to return FPC ID from component ID.
        """
        if use_local_storage:
            fpc_db_id = self.get_fpc_db_id_from_db(fpc_component_name)
        else:
            fpc_db_id = self.get_fpc_db_id_from_local_storage(fpc_component_name)

        return fpc_db_id


    def get_fpc_db_id_from_db(self, fpc_component_name):
        """Function to return FPC ID from database.
        """
        # print("Looking for ID of FPC " + str(fpc_component_name))

        fpc_db_id = -999
        # Set type id
        self.set_fpc_type_id()

        fpcs = self.db.list_component_of_type(self.type_id_fpc)
        for fpc in fpcs:
            component_id = fpc["ComponentID"]
            if component_id == fpc_component_name:
                fpc_db_id = fpc["ID"]

        return fpc_db_id

    def get_fpc_db_id_from_local_storage(self, fpc_component_name):
        """Function to return FPC ID from locally stored data.
        Need to update data from DB, but should save time.
        """
        print("Looking for ID of FPC " + str(fpc_component_name))
        fpc_db_id = -999
        input_file = open(self.local_storage_file_path)
        fpcs = input_file.read()
        input_file.close()

        fpcs = ast.literal_eval(fpcs)
        for fpc in fpcs:
            component_id = fpc["ComponentID"]
            if component_id == fpc_component_name:
                fpc_db_id = fpc["ID"]

        return fpc_db_id
