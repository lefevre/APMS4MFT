# imports I understand why they are here
from zeep import Client
from zeep.transports import Transport
# 2.7-ism
# import cookielib 
# 3-ism
import http.cookiejar
import datetime
from datetime import datetime
import sys
import subprocess
import configparser
import os
import re  # parse regular expression
import base64
import logging
# import cern_sso
import requests

# here are the cookies
OSTYPE = sys.platform
base_dir = os.getcwd()
SSO = base_dir + '/cookie/ssocookie.txt'


class APMSdatabase:
    def __init__(self,
                 project,
                 project_id,
                 userid,
                 wsdl,
                 sso=SSO):

        self.set_logger()
        self.baseID = project_id

        self.myLogger.info(sso)
        self.myLogger.info(project_id)
        self.myLogger.info(wsdl)

        # get-sso-cookie differs according to OS type
        if "darwin" in OSTYPE:
            subprocess.call(["cern-get-sso-cookie.py", "--kerberos", "--url", wsdl, "-o", sso])
        elif "linux" in OSTYPE:
            subprocess.call(["cern-get-sso-cookie", "--kerberos", "--url", wsdl, "-o", sso])
        else:
            subprocess.call(["cern-get-sso-cookie", "--krb", "-r", "-u", wsdl, "-o", sso])

        #

        # 2.7-ism
        #    jar=cookielib.MozillaCookieJar(sso)
        jar = http.cookiejar.MozillaCookieJar(sso)
        jar.load()
        transport = Transport(cache=False)
        transport.session.cookies.update(jar)

        self.DB = Client(wsdl, transport=transport)

        project_list = [p for p in self.DB.service.ProjectRead()]
        self.myLogger.debug(project_list)

    def set_logger(self):
        log_format = "%(funcname)-12s %(asctime)-24s %(process)d %(levelname)-6s %(message)s"
        logging.basicConfig(format=log_format, level=logging.DEBUG)
        self.myLogger = logging.getLogger()

    # activities related methods

    # activities type methods
    # list all the activities type in the base.
    def list_activities_types(self):
        activities = []
        activities = self.DB.service.ActivityTypeRead(self.baseID)
        self.myLogger.debug(activities)
        return activities

    # return all the component of one activity type
    def activity_type_read_all(self, which):
        one_activity = self.DB.service.ActivityTypeReadAll(which)
        self.myLogger.debug(one_activity)
        return one_activity

    # list all activities of a the type "which"
    def list_activities_of_type(self, which):
        list_activities = []
        list_activities = [self.DB.service.ActivityRead(self.baseID, which)]
        self.myLogger.debug(list_activities)
        return list_activities

    # activity oriented method
    # return  one  activity
    def read_one_activity(self, which):
        one_activity = self.DB.service.ActivityReadOne(which)
        self.myLogger.debug(one_activity)
        return one_activity

    def create_new_activity(self, activity_type_id, location, lot, activity_name, start, stop, position, result, status,
                            user):
        """Create activity in database.
        """
        param = [activity_type_id, location, lot, activity_name, start, stop, position, result, status, user]
        self.myLogger.debug(param)
        if self.activity_name_exists(activity_type_id, activity_name):
            print("Activity with this name is already in database. New activity with this name will not be created."
                  " Returning object of the existing activity instead.")
            new_activity = self.find_activity_with_name(activity_type_id, activity_name)
        else:
            print("Creating activity with name " + str(activity_name))
            new_activity = self.DB.service.ActivityCreate(activity_type_id, location, lot, activity_name,
                                                          start, stop, position, result, status, user)
            if new_activity['ErrorCode'] == -1:
                print(new_activity['ErrorMessage'])
        self.myLogger.debug(new_activity)

        return new_activity

    def activity_name_exists(self, activity_type_id, activity_name):
        """Check, if activity with this name is already in database.
        """
        list_of_activities_of_one_type = self.DB.service.ActivityRead(self.baseID, activity_type_id)
        if list_of_activities_of_one_type is None:
            return False
        else:
            for activity in list_of_activities_of_one_type:
                if activity["Name"] == activity_name:
                    return True
                else:
                    return False

    def find_activity_with_name(self, activity_type_id, activity_name):
        """Return object of activity with this name in database.
        """
        list_of_activities_of_one_type = self.DB.service.ActivityRead(self.baseID, activity_type_id)
        if list_of_activities_of_one_type is None:
            return None
        else:
            for activity in list_of_activities_of_one_type:
                if activity["Name"] == activity_name:
                    return activity
                else:
                    return None

    def find_activity_id_with_name(self, activity_type_id, activity_name):
        """Return ID in database of activity with this name.
        """
        list_of_activities_of_one_type = self.DB.service.ActivityRead(self.baseID, activity_type_id)
        if list_of_activities_of_one_type is None:
            return None
        else:
            for activity in list_of_activities_of_one_type:
                if activity["Name"] == activity_name:
                    return activity["ID"]
                else:
                    return None

    def activity_change(self, activity_id, activity_type_id, location, lot, activity_name, start, stop, position,
                        result, status, user):
        """Change activity data, but not components nor parameters, which have specific methods.
        """
        param = [activity_id, activity_type_id, location, lot, activity_name, start, stop, position, result, status,
                 user]
        self.myLogger.debug(param)
        changed_activity = self.DB.service.ActivityChange(activity_id, activity_type_id, location, lot, activity_name,
                                                          start, stop, position, result, status, user)
        if changed_activity['ErrorCode'] == -1:
            print(changed_activity['ErrorMessage'])

        return changed_activity

    # BLOCK TO HANDLE ACTIVITY'S URI
    def activity_uri_create(self, activity_db_id, uri_path, description, user):
        """Create and attach uri_path to activity_db_id.
        description is string, which can be empty.
        user is id of user in db (set -1 if unknown).
        """
        if self.check_uri(activity_db_id, uri_path):
            output_message = "Activity's URI was not created."
        else:
            res = self.DB.service.ActivityUriCreate(activity_db_id, uri_path, description, user)
            output_message = "Activity;s URI created uccessfully."
            if res['ErrorCode'] == -1:
                print(res['ErrorMessage'])
                output_message = "Activity's URI was not created due to error."
        return output_message

    def activity_uri_change(self, uri_db_id, uri_path, description, user):
        """Change URI uri_path in the db using its id in db uri_db_id.
        Should be original and attached to certain activity.
        description is string, which can be empty.
        user is id of user in db (set -1 if unknown)."""
        res = self.DB.service.ActivityUriChange(uri_db_id, uri_path, description, user)
        if res['ErrorCode'] == -1:
            print(res['ErrorMessage'])
        return res

    def activity_uri_remove(self, uri_db_id):
        """Remove URI from the database using its db id uri_db_id.
        This means it removes it from a certain activity.
        """
        res = self.DB.service.ActivityUriRemove(uri_db_id)
        return res

    def check_uri(self, activity_db_id, uri_path):
        """Check if activity already has the same uri
        """
        activity = self.read_one_activity(activity_db_id)
        uris = activity["Uris"]
        if uris is not None:
            activity_uris = activity["Uris"]["ActivityUri"]
            for uri in activity_uris:
                if uri["Path"] == uri_path:
                    print("This activity already has this uri path.")
                    return True
                else:
                    return False

    def activity_attachment_create(self, which, attachment_category, what, what_file_name, user):
        err_code = self.DB.service.ActivityAttachmentCreate(which, attachment_category, what, what_file_name, user)
        print("activityAttachmentCreate err code ->", err_code)
        return (err_code)

    # should not be of great use, remove not implemented unless real use demonstrated
    # buggus for an unknonw reason, maybe user id... stop looking for that.

    def activity_attachment_remove(self, which_attachment):
        res = self.DB.service.ActivityAttachmentRemove(which_attachment)
        print("activityAttachmentRemove err code ->", res)
        return (res)

    def activity_component_assign(self, component_id, activity_id, component_type_id, user):
        """Assign component to activity in db.
        component_id is id of component in db.
        activity_id is id of activity in db.
        component_type_id is id of this component type in db.
        user is id of user in db.
        """
        param = [component_id, activity_id, component_type_id, user]
        self.myLogger.debug(param)
        res = self.DB.service.ActivityComponentAssign(component_id, activity_id, component_type_id, user)
        if res['ErrorCode'] == -1:
            print(res['ErrorMessage'])
            print("Close the activity if you want to attach this component to other activity!")
        return res

    def activity_component_remove(self, activity_component_link):
        res = self.DB.service.ActivityComponentRemove(activity_component_link)
        return (res)

    # boggus, no idea why, conform with API doc is ok. what is the use of whoID2 ?
    # Bug found, in doc whoID1 and who ID2 are swapped.
    # whoID1 is project member ID
    # whoID2 is Person ID
    def activity_member_assign(self, which, who_id_1, who_id_2, leader_flag):
        self.myLogger.debug(which, who_id_1, who_id_2, leader_flag)

        err_code = self.DB.service.ActivityMemberAssign(who_id_1, which, who_id_2, leader_flag)
        self.myLogger.debug(err_code)
        return (err_code)

    # change status of member leader or not. Seems of little use
    # leader -> 1 , non leader -> 0

    def activity_member_change(self, relationship_id, leader_flag, user_id):
        err_code = self.DB.service.ActivityMemberChange(relationship_id, leader_flag, user_id)
        return (err_code)

    # probably of little use
    def activity_member_remove(self, relationship_id):
        err_code = self.DB.service.ActivityMemberRemove(relationship_id)
        return (err_code)

    # contrary as what API doc says, valueID is a string with a decimal number meaning
    def activity_parameter_change(self, value_id, value, user_id):
        err_code = self.DB.service.ActivityParameterChange(value_id, value, user_id)
        return (err_code)

    def activity_parameter_create(self, activity_db_id, activity_parameter_type_id, value, user):
        """Create and attach a parameter to activity.
        If parameter already exists, it is replaced with a new one.
        activity_db_id is the database activity id.
        activity_parameter_type_id is id related to parameter type of the specific activity (not a general db type id).
        value is a value of the parameter. Has to be a number! No string characters are allowed!
        user is id of user in db.
        """
        self.check_and_delete_parameter(activity_db_id)
        res = self.DB.service.ActivityParameterCreate(activity_db_id, activity_parameter_type_id, value, user)
        if res['ErrorCode'] == -1:
            print(res['ErrorMessage'])
        return res

    def activity_parameter_remove(self, activity_parameter_db_id):
        """Remove parameter and its value attached to activity.
        activity_parameter_db_id is database id related to parameter.
        """
        res = self.DB.service.ActivityParameterRemove(activity_parameter_db_id)
        if res['ErrorCode'] == -1:
            print(res['ErrorMessage'])
        return res

    def check_and_delete_parameter(self, activity_db_id):
        """Check if activity has any parameter. In positive case, removes it.
        """
        activity = self.read_one_activity(activity_db_id)
        parameters = activity["Parameters"]
        if parameters is not None:
            activity_parameters = activity["Parameters"]["ActivityParameter"]
            for parameter in activity_parameters:
                print("This activity already has a parameter "
                      + str(parameter["ActivityTypeParameter"]["Parameter"]["Name"])
                      + ". This will be replaced with new one.")
                self.activity_parameter_remove(parameter["ID"])
        return 1

    # component related methods

    # returns the list of the component types
    def list_of_component_type(self):
        components_types_list = []
        components_types_list = self.DB.service.ComponentTypeRead(self.baseID)
        self.myLogger.debug(components_types_list)
        return components_types_list

    # return the descriptor of one component type
    def show_component_type(self, component_type_id):
        component_description_list = []
        component_description_list = self.DB.service.ComponentTypeReadAll(component_type_id)
        self.myLogger.debug(component_description_list)
        return component_description_list

    def list_component_of_type(self, component_type_id):
        """Returns the list of component of the specified type.
        """
        component_list = self.DB.service.ComponentRead(self.baseID, component_type_id)
        self.myLogger.debug(component_list)
        return component_list

    # return one component

    def show_component(self, which, which_name):
        # whichName is the string name of the component
        # the call to ComponentReadOne works with an empty string
        # but the parameter is mandatory. The web interface needs it full name.

        component = []
        component = self.DB.service.ComponentReadOne(which, which_name)
        self.myLogger.debug(component)
        return component

    def attachment_category_read(self):
        res = self.DB.service.AttachmentCategoryRead()
        return (res)

    # create a new component
    def new_component(self, which_type, component_name, supplier, description, lot_id, package, user_id):
        # self.myLogger.info("new component", whichType,componentName,supplier, description, lotID,package, userID)
        res = self.DB.service.ComponentCreate(which_type, component_name, supplier, description, lot_id, package,
                                              user_id)
        # self.myLogger.debug(res)
        return res

    def change_component(self, which, which_type, component_name, supplier, description, lot_id, package, user_id):
        res = self.DB.service.ComponentChange(which, which_type, component_name, supplier, description, lot_id, package,
                                              user_id)
        return (res)

    # destroy one component
    def component_remove(self, which):
        res = self.DB.service.ComponentRemove(which)
        return (res)

    # warning the same component can be used several time at different positions
    # position can either be string or integer.
    def place_component_in_composition(self, host, element, position, user_id):
        param = [host, element, position, user_id]
        self.myLogger.debug(param)
        res = self.DB.service.ComponentCompositionCreate(host, element, position, user_id)
        return (res)

    # replace the component in a composition
    # newPos is of type STRING , integer won't work.
    # componentInComposition is the component tag found in host component.
    def component_composition_position_change(self, component_in_composition, new_pos, user):
        param = [component_in_composition, new_pos, user]
        self.myLogger.debug(param)
        res = self.DB.service.ComponentCompositionPositionChange(component_in_composition, new_pos,
                                                                 user)
        # IDcomponentInComposition,string of position, operator (-1 is ok)
        return (res)

    def remove_component_composition(self, component_in_composition):
        res = self.DB.service.ComponentCompositionRemove(component_in_composition)
        return (res)

    # returns the component composing the object given as parameter
    def list_composition_of(self, which):
        res = self.DB.service.ComponentChildrenRead(which)
        return (res)

    def component_activity_history_read(self, which):
        res = self.DB.service.ComponentActivityHistoryRead(which)
        return (res)

    def component_parent_read(self, which):
        res = self.DB.service.ComponentParentRead(which)
        return (res)

    # liste the component of type componentInType that are parts of composition in
    # component of type assembledInType.
    # Doc says taht the assembledInType is a string, but it works as int,  componentInType an int
    def list_component_of_type_in_composition_with_type(self, component_in_type, assembled_in_type):
        res = self.DB.service.ReadComponentWithParent(component_in_type, assembled_in_type)
        return (res)

    def project_read(self):
        project_list = self.DB.service.ProjectRead()
        self.myLogger.debug(project_list)
        return project_list

    def project_member_list(self):
        res = self.DB.service.ProjectMemberRead(self.baseID)
        return (res)

# is class do not need main
# if __name__ == '__main__':
#    rc = main()
#    sys.exit(rc)
