#!/usr/bin/python

#Récupère la liste des composants d'après  

#Test de killian

from zeep import Client
from zeep.transports import Transport
import http.cookiejar
import datetime
import sys 
import subprocess 
import configparser
import os
import re # parse regular expression 
import base64
import logging
from APMSdatabase import APMSdatabase 
from setAPMSenv import SetApmsEnv
import lxml


def setLogger():
    global myLogger
    logFormat="%(funcName)-12s %(asctime)-24s %(process)d %(levelname)-6s %(message)s"
    logging.basicConfig(format=logFormat,level=logging.DEBUG)
    myLogger=logging.getLogger()
    

def fctSortDict(value):
    return value['Id']

def main():
  setEnv=SetApmsEnv.SetApmsEnv()
  setLogger()

  envDict=setEnv.setup_environment_dict

  DATABASE=envDict.get("DATABASE")
  PROJECT=envDict.get("PROJECT")
  PROJECT_ID=int(envDict.get("PROJECT_ID"))
  USERID=int(envDict.get("USERID"))
  WSDL=envDict.get("WSDL")
  LOCATION=envDict.get("LOCATION")



  APMStest=APMSdatabase.APMSdatabase(PROJECT,PROJECT_ID,USERID,WSDL)


  print ("select Component type:")
  What=sys.stdin.readline()
  print ("list of all components :")
  res=APMStest.list_component_of_type(What)
  print(res)
  
  #Permet de filtrer la liste obtenu dans res.
  listTrier=[]
  for component in res:
    #La liste est constituée de plusieurs dictionnaire
    dico={}
    lId=component["ID"]
    leNom=component["ComponentID"]
    #Permet de supprimer ls espaces dans leNom:
    leNom=leNom.replace(" ","")
    #Chaque dictionnaire dispose de clé "Id" et "Nom"
    dico["Id"]=lId
    dico["Nom"]=leNom
    listTrier.append(dico)
    listTrier=sorted(listTrier,key=fctSortDict) #appel la fonction de trie
  print(listTrier)  
  
  sys.exit()
  
  
if __name__=='__main__':
  rc=main()
  sys.exit(rc)
