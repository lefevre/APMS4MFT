#!/usr/bin/python

# imports I understand why they are here
from zeep import Client
from zeep.transports import Transport
import http.cookiejar
import datetime
from datetime import datetime
import sys 
import subprocess 
import configparser
import os
import re # parse regular expression 
import base64
import logging
from APMSdatabase import APMSdatabase 
from setAPMSenv import SetApmsEnv
import lxml
import array 


def setLogger():
    global myLogger
    logFormat="%(funcName)-12s %(asctime)-24s %(process)d %(levelname)-6s %(message)s"
    logging.basicConfig(format=logFormat,level=logging.DEBUG)
    myLogger=logging.getLogger()



def main():
  setEnv=SetApmsEnv.SetApmsEnv()
  setLogger()
  LOCATION=setEnv.setupLocationFile()
  print('setup found:', LOCATION)
  APMStest=APMSdatabase.APMSdatabase()

  APMStest.foo(1)

#  myLogger.info(LOCATION) 

#  myLogger.info(APMStest.baseID)
#  myLogger.info(APMStest)
#  listActivitesTypes=APMStest.listActivitiesTypes()
#  myLogger.info(listActivitesTypes)
 

# user ID 
# FL personID=4807 , userID = 1085
# Stefano personID=10068 userID = 1101
  userID=-1
  personID=10068
  userLeaderID=0
 
# this is to change a chip postion in a HicGlued  
  gluedAlpideID=34273
  newPos="A1"

# this is to set a new activity in the APMS database 
  activityType=949
  where=850
  lot=None

#test for attchment cat# low use 
  AttachmentCategoryID=163 #generic report 
  AttachmentFileName="oneFileSample"
  AttachmentContentString="CAC01CAFE"
  AttachmentContentByte=bytearray()
  AttachmentContentByte.extend(map(ord,AttachmentContentString))


  activityName='Hic3_Bound_S3_1_Visu_Chk_python_8'
 

  start='05.11.2018'
  stop='05.11.2018'
  position=None
# result 924 : passed ; result 925: failed (from ActivityTypeReadAll)
  result=924
# status open == 452 close == 453 
  status=452
#   who=4807 is PersonID, 1085 is ID in project member list   ID : 1643
# 1085 -> void field in database let's try 4807 ; idem 4807
  who=1085

#  APMStest.newActivity(activityType,where, lot, activityName,start, stop, position, result, status, who)


  APMStest.component_composition_position_change(gluedAlpideID, newPos, userID)

#  APMStest.activityComponentAssign(123226,95522,1331,-1) # assign In component
#  APMStest.activityComponentAssign(123226,95522,1346,-1) # assign Out component


  sys.exit()

if __name__=='__main__':
  rc=main()
  sys.exit(rc)
