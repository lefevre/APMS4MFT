#This script filter data differences betwenn ITS-Production and MFT

from zeep import Client
from zeep.transports import Transport
import http.cookiejar
import sys 
import subprocess 
import configparser
import os
import re # parse regular expression 
import base64
import logging
from APMSdatabase import APMSdatabase 
from setAPMSenv import SetApmsEnv
import lxml
import datetime
import ast


def setLogger():
    global myLogger
    logFormat="%(funcName)-12s %(asctime)-24s %(process)d %(levelname)-6s %(message)s"
    logging.basicConfig(format=logFormat,level=logging.DEBUG)
    myLogger=logging.getLogger()
    
  
def fctSortDict(value):
    return value["ComponentID"]
  
  
    
def main():
#Set the environment (configuration files are in the folder: "config")
  setEnv=SetApmsEnv.SetApmsEnv()
  setLogger()

  envDict=setEnv.setup_environment_dict

  DATABASE=envDict.get("DATABASE")
  PROJECT=envDict.get("PROJECT")
  PROJECT_ID=int(envDict.get("PROJECT_ID"))
  USERID=int(envDict.get("USERID"))
  WSDL=envDict.get("WSDL")
  LOCATION=envDict.get("LOCATION")
  CHIP=envDict.get("ALPIDEB-CHIP")
  
  APMStest=APMSdatabase.APMSdatabase(PROJECT,PROJECT_ID,USERID,WSDL)

#Reading the file who contains all Chips
  APMStest=APMSdatabase.APMSdatabase(PROJECT,PROJECT_ID,USERID,WSDL)
  fichier = open("DataFile/ChipsInMFT.txt", "r")
  laListe=fichier.read()
  if len(laListe)!=0:
    laListe=ast.literal_eval(laListe)
  fichier.close()
  laListe=sorted(laListe[0],key=fctSortDict)
  print(laListe[0])
  listResult=[]
  listeComponents=[]
  What=CHIP
#API Querying
  res=APMStest.list_component_of_type(What)

#Sorting the list
  listeComponents=sorted(res,key=fctSortDict)


#At this point we have two list of dictionnary which are sort by their ID.
  a=0
#This loop will determine the new components to integrate in database
  for i in range (len(listeComponents)):
    print(i+a)
    #This condition adjust the loop trail 
    if listeComponents[i]["ComponentID"]!=laListe[i-a]["ComponentID"]:
      a=a+1
      listResult.append(listeComponents[i])
      print("i: "+str(listeComponents[i]["ID"]))
      print("a+i: "+str(laListe[i]["ID"]))
  
  #Save result in a file
  file=open("DataToAdd/ChipsToAdd.txt","w")
  file.write(str(listResult))
  file.close()
  
  sys.exit()
  

if __name__=='__main__':
  rc=main()
  sys.exit(rc)
