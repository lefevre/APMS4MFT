"""This module is intended to contain all methods related to ladder.
"""

__author__ = "Roman Lavicka"

# standard classes
import ast
import os
import sys

# local classes
from setAPMSenv import SetApmsEnv
from APMSdatabase import APMSdatabase

# global variables for this module
use_local_storage = False
local_storage_file_path = ""
environment_setup = SetApmsEnv.SetApmsEnv()
environment_dict = environment_setup.setup_environment_dict
type_id_ladder = -1
n_chips = -1
db = APMSdatabase.APMSdatabase(
        environment_dict.get("PROJECT"),
        int(environment_dict.get("PROJECT_ID")),
        int(environment_dict.get("USERID")),
        environment_dict.get("WSDL"),
        )

class LadderManager:
    """Manager to control ladders in database.
    """

    def __init__(self, environment_setup_, n_chips_, use_local_storage_):
        self.environment_setup = environment_setup_
        self.environment_dict = environment_setup.setup_environment_dict
        self.db = APMSdatabase.APMSdatabase(
                environment_dict.get("PROJECT"),
                int(environment_dict.get("PROJECT_ID")),
                int(environment_dict.get("USERID")),
                environment_dict.get("WSDL"),
                )
        self.local_storage_file_path = "localData/LaddersInMFT.txt"
        print(self.switch_data_storage(use_local_storage_))
        self.type_id_ladder = -1
        self.n_chips = n_chips_
        self.set_ladder_type_id()
        return

    def set_ladder_type_id(self):
        """Set the correct ladder type id with respect to number of chips on the ladder
        """
        try:
            int(self.n_chips)
        except ValueError:
            sys.exit("\n****** ERROR: Number of chips is not an integer! ******")

        if int(self.n_chips) == 2:
            self.type_id_ladder = self.environment_dict.get("LADDER-2")
            return 2
        elif int(self.n_chips) == 3:
            self.type_id_ladder = self.environment_dict.get("LADDER-3")
            return 3
        elif int(self.n_chips) == 4:
            self.type_id_ladder = self.environment_dict.get("LADDER-4")
            return 4
        elif int(self.n_chips) == 5:
            self.type_id_ladder = self.environment_dict.get("LADDER-5")
            return 5
        else:
            sys.exit("\n****** ERROR: Invalid number of chips! ******")

    def switch_data_storage(self, on_off):
        """Function to switch between local storage and other.
        If you switch it on, local database is actualized.
        """
        message = "Ladder Manager set to use remote database."
        global use_local_storage

        use_local_storage = on_off

        if use_local_storage:
            message = "Ladder Manager set to use local database."
            self.store_locally_all_ladders()

        return message

    def store_locally_all_ladders(self):
        """Function to store all info on ladder components from DB locally.
        """
        # Remove the file.
        try:
            os.remove(self.local_storage_file_path)
        except:
            sys.exit("Error while deleting file : " + str(self.local_storage_file_path))

        # Set type id
        self.set_ladder_type_id()

        # API Querying
        list_of_components = self.db.list_component_of_type(self.type_id_ladder)

        # Save result in a file
        file = open(self.local_storage_file_path, "w")
        file.write(str(list_of_components))
        file.close()

        return list_of_components

    def remove_ladder_with_component_id(self, ladder_component_id):
        """Function to remove ladder using its component ID
        """
        message = "Ladder not found."
        ladder_db_id = self.get_ladder_db_id_from_component_id(ladder_component_id)
        if ladder_db_id != -999:
            removed = self.db.component_remove(ladder_db_id)
            if removed["ErrorCode"] == -1:
                message = "Ladder " + str(ladder_component_id) + " found but in use. Cannot be deleted."
            else:
                message = "Ladder " + str(ladder_component_id) + " found and will be deleted."
        print(message)

    def create_new_ladder(self, ladder_component_id):
        """Function to create ladder and return its ID.
        Should contain all safeguards.
        """
        print("Creating new ladder " + str(ladder_component_id))

        # Set type id
        self.set_ladder_type_id()

        new_ladder = self.db.new_component(self.type_id_ladder, ladder_component_id, "", "", "", "", -1)
        ladder_db_id = new_ladder["ID"]

        if use_local_storage:
            self.store_locally_all_ladders()

        # if ladder already exists in database, new_ladder["ID"] returns None. This should return its ID
        if ladder_db_id is None:
            ladder_db_id = self.get_ladder_db_id_from_component_id(ladder_component_id)

        return ladder_db_id

    def get_ladder_db_id_from_component_id(self, ladder_component_id):
        """Function to return ladder database id using its component ID.
        """
        print("Looking for ID of ladder " + str(ladder_component_id))
        if use_local_storage:
            ladder_db_id = self.get_ladder_db_id_from_local_storage(ladder_component_id)
        else:
            ladder_db_id = self.get_ladder_db_id_from_database(ladder_component_id)
        return ladder_db_id

    def get_ladder_db_id_from_database(self,ladder_component_id):
        """Function to return ladder db ID from database.
        """
        ladder_db_id = -999
        # Set type id
        self.set_ladder_type_id()

        ladders = self.db.list_component_of_type(self.type_id_ladder)
        for ladder in ladders:
            component_id = ladder["ComponentID"]
            if component_id == ladder_component_id:
                ladder_db_id = ladder["ID"]
        return ladder_db_id

    def get_ladder_db_id_from_local_storage(self,ladder_component_id):
        """Function to return Ladder ID from locally stored data
        Need to update data from DB, but should save time.
        """
        ladder_db_id = -999
        input_file = open(self.local_storage_file_path)
        ladders = input_file.read()
        input_file.close()

        ladders = ast.literal_eval(ladders)
        for ladder in ladders:
            component_id = ladder["ComponentID"]
            if component_id == ladder_component_id:
                ladder_db_id = ladder["ID"]
        return ladder_db_id
