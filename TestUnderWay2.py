#!/usr/bin/python

# imports I understand why they are here
from zeep import Client
from zeep.transports import Transport
import http.cookiejar
import datetime
from datetime import datetime
import sys 
import subprocess 
import configparser
import os
import re # parse regular expression 
import base64
import logging
import lxml
from setAPMSenv import SetApmsEnv
from APMSdatabase import APMSdatabase 



def setLogger():
    global myLogger
    logFormat="%(funcName)-12s %(asctime)-24s %(process)d %(levelname)-6s %(message)s"
    logging.basicConfig(format=logFormat,level=logging.DEBUG)
    myLogger=logging.getLogger()



def main():


  setEnv=SetApmsEnv.SetApmsEnv()
  setLogger()
  envDict=setEnv.setup_environment_dict
  print("setup found:", envDict)


  DATABASE=envDict.get("DATABASE")
  PROJECT=envDict.get("PROJECT")
  PROJECT_ID=int(envDict.get("PROJECT_ID"))
  USERID=int(envDict.get("USERID"))
  WSDL=envDict.get("WSDL")
  LOCATION=envDict.get("LOCATION")


  APMStest=APMSdatabase.APMSdatabase(PROJECT,PROJECT_ID,USERID,WSDL)


#  listActivitesTypes=APMStest.listActivitiesTypes()
#  myLogger.info(listActivitesTypes)
#  componentType=367 
#  res=APMStest.showComponentType(componentType)
# this is to set a new activity in the APMS database 
#  activityType=949
#  where=850
#  lot=None
#  activityName='Hic3_Bound_S3_1_Visu_Chk_python_4'
#  start='02.03.2018'
#  stop='02.03.2018'
#  position=None
# result 924 : passed ; result 925: failed (from ActivityTypeReadAll)
#  result=924
# status open == 452 
#  status=452
#   who=4807 is PersonID, 1085 is ID in project member list   ID : 1643
# 1085 -> void field in database let's try 4807 ; idem 4807
  who=1643

#  APMStest.newActivity(activityType,where, lot, activityName,start, stop, position, result, status, who)

#  alpideType=566  #in MFT-test
  alpideType=967 # in MFT-production 

  alpideName="KillianTest4"
  alpideSupplier="none"
  description="another alpide chip"
  lotID="1"
  package="1"

#  res=APMStest.newComponent(alpideType,alpideName,alpideSupplier,description,lotID,package,USERID)
#  print('new component ret :',res)

  projectList=APMStest.project_read()
  print(projectList)

  ListOfComponent=APMStest.list_of_component_type()
  print(ListOfComponent)


  sys.exit()

if __name__=='__main__':
  rc=main()
  sys.exit(rc)
