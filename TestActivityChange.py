#!/usr/bin/python

# imports I understand why they are here
from zeep import Client
from zeep.transports import Transport
from zeep import helpers
import http.cookiejar
import datetime
from datetime import datetime
import sys 
import subprocess 
import configparser
import os
import re # parse regular expression 
import base64
import logging
import lxml
from setAPMSenv import SetApmsEnv
from APMSdatabase import APMSdatabase 



def setLogger():
    global myLogger
    logFormat="%(funcName)-12s %(asctime)-24s %(process)d %(levelname)-6s %(message)s"
    logging.basicConfig(format=logFormat,level=logging.DEBUG)
    myLogger=logging.getLogger()



def main():


  setEnv=SetApmsEnv.SetApmsEnv()
  setLogger()
  envDict=setEnv.setup_environment_dict()
  print("setup found:", envDict)


  DATABASE=envDict.get("DATABASE")
  PROJECT=envDict.get("PROJECT")
  PROJECT_ID=int(envDict.get("PROJECT_ID"))
  USERID=int(envDict.get("USERID"))
  WSDL=envDict.get("WSDL")
  LOCATION=envDict.get("LOCATION")


  APMStest=APMSdatabase.APMSdatabase(PROJECT,PROJECT_ID,USERID,WSDL)


#  myLogger.info(LOCATION) 

  myLogger.info(APMStest.baseID)
  myLogger.info(APMStest)
#  listActivitesTypes=APMStest.listActivitiesTypes()
#  myLogger.info(listActivitesTypes)
 
#  componentType=367 
#  res=APMStest.showComponentType(componentType)


  #myLogger.info(res)


# this is to set a new activity in the APMS database 
  activityType=949
  where=850
  lot=None
  activityName='Hic3_Bound_S3_1_Visu_Chk_python_4'
  start='02.03.2018'
  stop='02.03.2018'
  position=None
# result 924 : passed ; result 925: failed (from ActivityTypeReadAll)
  result=924
# status open == 452 
  status=452
#   who=4807 is PersonID, 1085 is ID in project member list   ID : 1643
# 1085 -> void field in database let's try 4807 ; idem 4807
  who=1643
#  APMStest.newActivity(activityType,where, lot, activityName,start, stop, position, result, status, who)

  activityType=935
  activity=95662
  res=APMStest.read_one_activity(95662)
#  print(res)

#  print(type(res))
  actDic=helpers.serialize_object(res)
#  print("type of actDict==",type(actDic))
#  print(actDic)
  

  newName=actDic.get("Name")
  newLot=actDic.get("LotID")
  newStartDate=actDic.get("StartDate")
  newEndDate=actDic.get("EndDate")
  newPosition=actDic.get("Position") 
  newResult=actDic.get("ActivityResult")
  newStatus=actDic.get("ActivityStatus")
  newLocation=actDic.get("ActivityLocation")
  newUser=actDic.get("Members")

  print("type of position==",type(newPosition))
  print("newPosition==",newPosition)

  newName=newName+' modif'
  newLot="New Lot 1"
  newPosition="0"
# the best things are at CERN
  newLocation=819 


  print("type of newStartDate==", type(newStartDate))
  strStartDate=newStartDate
  newStartDate=datetime.strptime(strStartDate,"%d.%m.%Y") 
  strEndDate=newEndDate
  newEndDate=datetime.strptime(strEndDate,"%d.%m.%Y") 

  print("newName==", newName)
  print("newStartDate==", newStartDate)
#  print("members == ", newUser)

  newStartDate="30.12.2018"
  newEndDate="31.12.2018"

  # cat A , ok 
  newResult=893
  # let it open please 
  newStatus=452

#  newResult=-1
#  newStatus=-1

  #still unknown user 
  newUser=4807

  res=APMStest.activity_change(activity, activityType, newLocation, newLot, newName, newStartDate, newEndDate, newPosition, newResult, newStatus, newUser)

#  res=APMStest.activityChange(activity,activityType,newLocation,newLot,newName,newStartDate,newEndDate,newPosition,newResult,newStatus,newUser)

  print(res)

  sys.exit()

if __name__=='__main__':
  rc=main()
  sys.exit(rc)
