#This script create a file where are saved all component: Chips
from zeep import Client
from zeep.transports import Transport
import http.cookiejar
import sys 
import subprocess 
import configparser
import os
import re # parse regular expression 
import base64
import logging
from APMSdatabase import APMSdatabase 
from setAPMSenv import SetApmsEnv
import lxml
import datetime
import ast

# local scripts
import GetAllLadders



def setLogger():
    global myLogger
    logFormat="%(funcName)-12s %(asctime)-24s %(process)d %(levelname)-6s %(message)s"
    logging.basicConfig(format=logFormat,level=logging.DEBUG)
    myLogger=logging.getLogger()
    
    
def main(IdActivityType=935):
#Set the environment (configuration files are in the folder: "config")
  setEnv=SetApmsEnv.SetApmsEnv()
  envDict=setEnv.setup_environment_dict
  DATABASE=envDict.get("DATABASE")
  PROJECT=envDict.get("PROJECT")
  PROJECT_ID=int(envDict.get("PROJECT_ID"))
  USERID=int(envDict.get("USERID"))
  WSDL=envDict.get("WSDL")
  LOCATION=envDict.get("LOCATION")
  FPC=envDict.get("LADDER-3")
  APMStest=APMSdatabase.APMSdatabase(PROJECT,PROJECT_ID,USERID,WSDL)


#Get IDs from console
#  ladderID = int(input("insert ladder database ID (not component ID)"))
  ID_ladder = 201949
  SetAllChipPositionsOnLadder(ID_ladder, APMStest)





#   chipID = int(input("insert chip ID as a ladder component composition (not component ID)"))
#   chipPosition = str(input("insert chip position on a ladder"))
#
# #API Querying
#   APMStest.componentCompositionPositionChange(chipID, chipPosition, -1)
#
#
#  print(APMStest.showComponent(ladderID,""))

  sys.exit()

def SetAllChipPositionsOnLadder(ID_ladder, DB):
  print("Assigning positions of chips on ladder from hic traveler")
  #Refresh database of ladders
  #GetAllLadders.main()
  listLadder=[]
  ladderComposition=[]
  ######################
  # Workaround to get correct type
  res=DB.show_component(ID_ladder, "")
  listLadder.append(res)

  # Temporary save
  file = open("temp.txt", "w")
  file.write(str(listLadder))
  file.close()

  fInput = open("temp.txt","r")
  listLadder=fInput.read()#
  fInput.close()
  os.remove("temp.txt")
  #######################

#  print(type(listLadder))#str
  listLadder=ast.literal_eval(listLadder) #Change type to 'list'
#  print(type(listLadder))#list
  #print(listLadder)

#  thisLadder=listLadder[0]
  for layer0 in listLadder:
    ladderComposition=layer0["Composition"]
#    print(type(ladderComposition))#dict
#    print(ladderComposition)

  compositionComponents=ladderComposition["ComponentComposition"]
#  print(type(compositionComponents))#list
#  print(len(compositionComponents))
#  print(compositionComponents)

  for component in compositionComponents:
    currentComponent=component["Component"]
    currentComponentCompositionID=component["ID"]
    currentPosition=component["Position"]
    #print(type(currentComponent))#dict
    print(currentPosition)
    currentComponentType=currentComponent["ComponentType"]
    currentComponentID=currentComponent["ComponentID"]
    #print(type(currentComponentType))#dict

    # for componentInfo in currentComponent:
    #   currentComponentType=componentInfo["ComponentType"]
    if currentComponentType["ID"] == 967: # ask for ALPIDEB Chip
      chipID = currentComponentCompositionID
      print(chipID)
      chipPosition = str(input("insert chip position on a ladder for chip ID " + currentComponentID +"\n"))
      #API Querying
      DB.component_composition_position_change(chipID, chipPosition, -1)
      currentPosition=component["Position"]
      print(currentPosition)

    if currentComponentType["Name"] == "ALPIDEB Chip": # ask for ALPIDEB Chip
      print(currentPosition)

  return


if __name__=='__main__':
  rc=main()
  sys.exit(rc)
