#This script can be use to get all Status of an activity
  
  
from zeep import Client
from zeep.transports import Transport
import http.cookiejar
import sys 
import subprocess 
import configparser
import os
import re # parse regular expression 
import base64
import logging
from APMSdatabase import APMSdatabase 
from setAPMSenv import SetApmsEnv
import lxml
import datetime
import ast


def setLogger():
    global myLogger
    logFormat="%(funcName)-12s %(asctime)-24s %(process)d %(levelname)-6s %(message)s"
    logging.basicConfig(format=logFormat,level=logging.DEBUG)
    myLogger=logging.getLogger()


def main():
#Set the environment (configuration files are in the folder: "config")
  setEnv=SetApmsEnv.SetApmsEnv()
  envDict=setEnv.setup_environment_dict
  DATABASE=envDict.get("DATABASE")
  PROJECT=envDict.get("PROJECT")
  PROJECT_ID=int(envDict.get("PROJECT_ID"))
  USERID=int(envDict.get("USERID"))
  WSDL=envDict.get("WSDL")
  LOCATION=envDict.get("LOCATION")
  CHIP=envDict.get("ALPIDEB-CHIP")#367 pour ITSP
  TRAY=envDict.get("ALPIDEB-TRAY")#387 pour ITSP
  APMStest=APMSdatabase.APMSdatabase(PROJECT,PROJECT_ID,USERID,WSDL)
  
  
#Reading the file who contains all Activities
  
  fichier = open("DataToAdd/ActivityInformations.txt","r") #Chips.txt contains all Chips of one database. This file was create by GetAllChips.py
  liste=fichier.read()#here type(liste) = 'str'
  liste=ast.literal_eval(liste) #Change liste type to 'list'
  fichier.close()
  
  listeRes=[]
  dico={}
  lesStatus=liste["Location"]
  for un in lesStatus["ActivityTypeLocation"]:
    dico[un["Name"]]=un["ID"]
  listeRes.append(dico)
  
  print(listeRes)

  fichier=open("config/ITSLocation.txt","w")
  fichier.write(str(listeRes))
  fichier.close()
  
  #~ sys.exit()

if __name__=='__main__':
  rc=main()
  sys.exit(rc)
  
