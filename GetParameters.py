#This script can be use to get all result of an activity
  
  
from zeep import Client
from zeep.transports import Transport
import http.cookiejar
import sys 
import subprocess 
import configparser
import os
import re # parse regular expression 
import base64
import logging
from APMSdatabase import APMSdatabase 
from setAPMSenv import SetApmsEnv
import lxml
import datetime
import ast


def setLogger():
    global myLogger
    logFormat="%(funcName)-12s %(asctime)-24s %(process)d %(levelname)-6s %(message)s"
    logging.basicConfig(format=logFormat,level=logging.DEBUG)
    myLogger=logging.getLogger()


def main():
#Set the environment (configuration files are in the folder: "config")
  setEnv=SetApmsEnv.SetApmsEnv()
  envDict=setEnv.setup_environment_dict
  DATABASE=envDict.get("DATABASE")
  PROJECT=envDict.get("PROJECT")
  PROJECT_ID=int(envDict.get("PROJECT_ID"))
  USERID=int(envDict.get("USERID"))
  WSDL=envDict.get("WSDL")
  LOCATION=envDict.get("LOCATION")
  CHIP=envDict.get("ALPIDEB-CHIP")#367 pour ITSP
  TRAY=envDict.get("ALPIDEB-TRAY")#387 pour ITSP
  APMStest=APMSdatabase.APMSdatabase(PROJECT,PROJECT_ID,USERID,WSDL)
  
  
#Reading the file who contains all Activities
  
  fichier = open("DataToAdd/ActivityInformations.txt","r") #Chips.txt contains all Chips of one database. This file was create by GetAllChips.py
  liste=fichier.read()#here type(liste) = 'str'
  liste=ast.literal_eval(liste) #Change liste type to 'list'
  fichier.close()
  
  listeRes=[]
  dico={}
  lesResults=liste["Parameters"]
  print(liste)
  for un in lesResults["ActivityTypeParameter"]:
    dico[un["Parameter"]["Name"]]=un["Parameter"]["ID"]
  listeRes.append(dico)

  fichier=open("config/ITSParameters.txt","w")
  fichier.write(str(listeRes))
  fichier.close()  
  
      
  
  #~ laListe=[]
  #~ dico2={}
  #~ for k in range(len(listeRes)):
    #~ dico2[listeRes[k]["ParametreName"]]=listeRes[k]["ParametreID"]
  #~ laListe.append(dico2)
  
  

if __name__=='__main__':
  rc=main()
  sys.exit(rc)




