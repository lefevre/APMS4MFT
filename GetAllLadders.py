#This script create a file where are saved all component: Chips
from zeep import Client
from zeep.transports import Transport
import http.cookiejar
import sys 
import subprocess 
import configparser
import os
import re # parse regular expression 
import base64
import logging
from APMSdatabase import APMSdatabase 
from setAPMSenv import SetApmsEnv
import lxml
import datetime



def setLogger():
    global myLogger
    logFormat="%(funcName)-12s %(asctime)-24s %(process)d %(levelname)-6s %(message)s"
    logging.basicConfig(format=logFormat,level=logging.DEBUG)
    myLogger=logging.getLogger()
    
    
def main(IdActivityType=935):
#Set the environment (configuration files are in the folder: "config")
  #Set the environment (configuration files are in the folder: "config")
  setEnv=SetApmsEnv.SetApmsEnv()
  envDict=setEnv.setup_environment_dict
  DATABASE=envDict.get("DATABASE")
  PROJECT=envDict.get("PROJECT")
  PROJECT_ID=int(envDict.get("PROJECT_ID"))
  USERID=int(envDict.get("USERID"))
  WSDL=envDict.get("WSDL")
  LOCATION=envDict.get("LOCATION")
  TYPE=envDict.get("LADDER-3")
  APMStest=APMSdatabase.APMSdatabase(PROJECT,PROJECT_ID,USERID,WSDL)
  
  listComponentTypeID=[TYPE]
  listeComponents=[]
  
#API Querying
  for i in listComponentTypeID:
    What=i
    print(i)
    res=APMStest.list_component_of_type(What)
    listeComponents.append(res)
    # print(APMStest.showComponent(202027,""))

  # print(APMStest.listComponentOfType(What))
    
#Save result in a file
  file=open("localData/LaddersInMFT.txt","w")
  res=str(listeComponents)
  file.write(str(listeComponents))
  file.close()
  
  # sys.exit()
  

if __name__=='__main__':
  rc=main()
  sys.exit(rc)
