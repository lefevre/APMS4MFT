#This script create a file where are saved all component: Tray

from zeep import Client
from zeep.transports import Transport
import http.cookiejar
import sys 
import subprocess 
import configparser
import os
import re # parse regular expression 
import base64
import logging
from APMSdatabase import APMSdatabase 
from setAPMSenv import SetApmsEnv
import lxml
import datetime



def setLogger():
    global myLogger
    logFormat="%(funcName)-12s %(asctime)-24s %(process)d %(levelname)-6s %(message)s"
    logging.basicConfig(format=logFormat,level=logging.DEBUG)
    myLogger=logging.getLogger()
    
def fctSortDict(value):
    return value["ID"]
  

def main():
#Set the environment (configuration files are in the folder: "config")
  setEnv=SetApmsEnv.SetApmsEnv()
  setLogger()

  envDict=setEnv.setup_environment_dict

  DATABASE=envDict.get("DATABASE")
  PROJECT=envDict.get("PROJECT")
  PROJECT_ID=int(envDict.get("PROJECT_ID"))
  USERID=int(envDict.get("USERID"))
  WSDL=envDict.get("WSDL")
  LOCATION=envDict.get("LOCATION")
  CHIP=envDict.get("ALPIDEB-CHIP")#367 pour ITSP
  TRAY=envDict.get("ALPIDEB-TRAY")#387 pour ITSP
  print(TRAY)


  APMStest=APMSdatabase.APMSdatabase(PROJECT,PROJECT_ID,USERID,WSDL)
  fichier = open("DataFile/TraysInMFT.txt", "w")
  fichier.close()
  listComponentTypeID=[TRAY]#CHIP
  listeComponents=[]
  
#API Querying
  for i in listComponentTypeID:
    What=i
    print(i)
    res=APMStest.list_component_of_type(What)
    res=sorted(res,key=fctSortDict)
    listeComponents.append(res)
#Save result in a file
  file=open("DataFile/TraysInMFT.txt","w")
  res=str(listeComponents)
  file.write(str(listeComponents))
  file.close()
  
  sys.exit()
  

if __name__=='__main__':
  rc=main()
  sys.exit(rc)

