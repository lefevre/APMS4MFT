"""This module is intended to contain all methods related to documents manipulation.
"""

__author__ = "Roman Lavicka"

# standard classes
import os
import sys
import xmltodict

# global variables for this module
fpc_form_number = "24aA101"
fpc_form = []
is_hic_traveler = True
hic_traveler_number = 3154
hic_traveler = []

class DocumentsManager:
    """Manager to control documents collecting informations on ladders and FPCs in database.
    """

    def __init__(self, document_number, is_hic_traveler_=True):
        if is_hic_traveler_:
            self.is_hic_traveler = is_hic_traveler_
            self.hic_traveler_number = document_number
            self.hic_traveler = self.open_hic_traveler_number()
        else:
            self.is_hic_traveler = is_hic_traveler_
            self.fpc_form_number = document_number
            self.fpc_form = self.open_fpc_form_number()
            # print(is_hic_traveler,is_hic_traveler_,self.is_hic_traveler)

    def open_fpc_form_number(self):
        """Function to return FPC form as dictionary for a specific number.
        If .xml file is not in localData, it tries to downloads it.
        If it fails, should return error
        """
        fpc_form_path = "localData/travelers/FPC_" + str(self.fpc_form_number) + ".xml"
        curl_command = "curl -o " + str(fpc_form_path) + " --ntlm --user "
        username_ = "alicemft" + str(":")
        password_ = "'Wezu-Vate'"
        espace_url = " \"https://espace.cern.ch/alice-mft-wg2/_layouts/15/download.aspx?SourceUrl=" \
                     "%2Falice%2Dmft%2Dwg2%2FFPC%5FFORMS%2FFPC%5F" \
                     + str(self.fpc_form_number) + \
                     "%2Exml&FldUrl=&Source=httphttps://espace.cern.ch/alice-mft-wg2/_layouts/15/download.aspx" \
                     "?SourceUrl=%2Falice%2Dmft%2Dwg2%2FFPC%5FFORMS%2FFPC%5F24aA101%2Exml&FldUrl=&Source=" \
                     "https%3A%2F%2Fespace%2Ecern%2Ech%2Falice%2Dmft%2Dwg2%2FFPC%5FFORMS%2FForms%2FSynthesis%2Easpxs" \
                     "%3A%2F%2Fespace%2Ecern%2Ech%2Falice%2Dmft%2Dwg2%2FFPC%5FFORMS%2FForms%2FSynthesis%2Easpx\""

        if os.path.exists(fpc_form_path):
            with open(fpc_form_path) as fd:
                fpc_form_ = xmltodict.parse(fd.read())
                print("FPC form " + str(self.fpc_form_number) + " opened!")
        else:
            print("Downloading FPC form " + str(self.fpc_form_number))
            os.system(curl_command + username_ + password_ + espace_url)
            with open(fpc_form_path) as fd:
                fpc_form_ = xmltodict.parse(fd.read())
                if 'my:mesChamps' in fpc_form_:
                    print("FPC form " + str(self.fpc_form_number) + " opened!")
                else:
                    os.system("rm " + str(fpc_form_path))
                    sys.exit("****** ERROR: FPC form " + str(self.hic_traveler_number) +
                             " not found! I cannot continue. Script will be terminanted! ******")

        return fpc_form_

    def open_hic_traveler_number(self):
        """Function to return HIC traveler as dictionary for a specific number.
        If .xml file is not in localData, it tries to downloads it.
        If it fails, should return error
        """
        hic_traveler_path = "localData/travelers/HIC_" + str(self.hic_traveler_number) + ".xml"
        curl_command = "curl -o " + str(hic_traveler_path) + " --ntlm --user "
        username_ = "alicemft" + str(":")
        password_ = "'Wezu-Vate'"
        espace_url = " \"https://espace.cern.ch/alice-mft-wg2/_layouts/15/download.aspx?SourceUrl=" \
                     "%2Falice%2Dmft%2Dwg2%2FHIC%5FTRAVELERS%2FHIC%5F" \
                     + str(self.hic_traveler_number) + \
                     "%2Exml&FldUrl=&Source=https%3A%2F%2Fespace%2Ecern%2Ech%2Falice%2Dmft%2Dwg2%2F" \
                     "HIC%5FTRAVELERS%2FForms%2FSynthesis%2Easpx\""

        if os.path.exists(hic_traveler_path):
            with open(hic_traveler_path) as fd:
                hic_traveler_ = xmltodict.parse(fd.read())
                print("HIC traveler " + str(self.hic_traveler_number) + " opened!")
        else:
            print("Downloading HIC traveler " + str(self.hic_traveler_number))
            os.system(curl_command + username_ + password_ + espace_url)
            with open(hic_traveler_path) as fd:
                hic_traveler_ = xmltodict.parse(fd.read())
                if 'my:mesChamps' in hic_traveler_:
                    print("HIC traveler " + str(self.hic_traveler_number) + " opened!")
                else:
                    os.system("rm " + str(hic_traveler_path))
                    sys.exit("****** ERROR: HIC traveler " + str(self.hic_traveler_number) +
                             " not found! I cannot continue. Script will be terminanted! ******")

        return hic_traveler_

    def get_operator(self):
        """Function to return operator.
        """
        if self.is_hic_traveler:
            operator = self.hic_traveler['my:mesChamps']['my:HIC_ASSEM_OPER']
        else:
            operator = self.fpc_form['my:mesChamps']['my:FPC_RAW_OPER']

        return operator

    def get_activity_start_date(self):
        """Function to return start date.
        """
        if self.is_hic_traveler:
            date_and_time = self.hic_traveler['my:mesChamps']['my:HIC_ASSEM_DATE']
        else:
            date_and_time = self.fpc_form['my:mesChamps']['my:FPC_RAW_DATE']
        date = date_and_time[0:10]

        return date

    def get_uri(self):
        """Function to get uri of CERNbox.
        """
        if self.is_hic_traveler:
            uri = str(self.hic_traveler['my:mesChamps']['my:Link_Cernbox']['#text'])
            # ALTERNATIVE
            # base_uri = "https://cernbox.cern.ch/index.php/apps/files?dir=/__myprojects/" \
            #            "mft/PRODUCTION/WP2_Ladder/HIC_Ladder/HIC_Ladder_"
            # uri = base_uri + str(self.hic_traveler['my:mesChamps']['my:HICid']) + "&"
        else:
            uri = str(self.fpc_form['my:mesChamps']['my:FPC_cernbox']['#text'])

        return uri

    def get_location(self):
        """Returns location name as written in document.
        """
        if self.is_hic_traveler:
            location = self.hic_traveler['my:mesChamps']['my:HIC_LOC']
        else:
            location = self.fpc_form['my:mesChamps']['my:FPC_LOC']

        return location
