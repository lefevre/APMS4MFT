#Get activityType's informations.

from zeep import Client
from zeep.transports import Transport
import http.cookiejar
import sys 
import subprocess 
import configparser
import os
import re # parse regular expression 
import base64
import logging
from APMSdatabase import APMSdatabase
from setAPMSenv import SetApmsEnv
import lxml
import datetime
import ast


def setLogger():
    global myLogger
    logFormat="%(funcName)-12s %(asctime)-24s %(process)d %(levelname)-6s %(message)s"
    logging.basicConfig(format=logFormat,level=logging.DEBUG)
    myLogger=logging.getLogger()


def main(IdActivityType=935):
#Set the environment (configuration files are in the folder: "config")
  setEnv=SetApmsEnv.SetApmsEnv()
  envDict=setEnv.setup_environment_dict
  DATABASE=envDict.get("DATABASE")
  PROJECT=envDict.get("PROJECT")
  PROJECT_ID=int(envDict.get("PROJECT_ID"))
  USERID=int(envDict.get("USERID"))
  WSDL=envDict.get("WSDL")
  LOCATION=envDict.get("LOCATION")
  CHIP=envDict.get("ALPIDEB-CHIP")#367 pour ITSP
  TRAY=envDict.get("ALPIDEB-TRAY")#387 pour ITSP
  APMStest=APMSdatabase.APMSdatabase(PROJECT,PROJECT_ID,USERID,WSDL)

  liste=[]
#Api querying
  What=IdActivityType
  res=APMStest.activity_type_read_all(What)
  liste=res
  #print(paramsList)
  #print(liste)
  #lId=liste["Parameters"]
  #print(lId)
  
  
#Save result in a file
  fichier3=open("DataToAdd/ActivityInformations.txt","w")
  fichier3.write(str(liste))
  fichier3.close()  
  sys.exit()
  
  
if __name__=='__main__':
  rc=main()
  sys.exit(rc)
