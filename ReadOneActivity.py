#!/usr/bin/python

# imports I understand why they are here
from zeep import Client
from zeep.transports import Transport
import http.cookiejar
import datetime
from datetime import datetime
import sys 
import subprocess 
import configparser
import os
import re # parse regular expression 
import base64
import logging
from APMSdatabase import APMSdatabase 
from setAPMSenv import SetApmsEnv
import lxml


def setLogger():
    global myLogger
    logFormat="%(funcName)-12s %(asctime)-24s %(process)d %(levelname)-6s %(message)s"
    logging.basicConfig(format=logFormat,level=logging.INFO)
    myLogger=logging.getLogger()



def main():
  setEnv=SetApmsEnv.SetApmsEnv()
  setLogger()
  LOCATION=setEnv.setupLocationFile()
  print('setup found:', LOCATION)
  APMStest=APMSdatabase.APMSdatabase()

  print ("select activity:")
  What=sys.stdin.readline()

#  listActivitesTypes=APMStest.listActivitiesTypes()
#  myLogger.info(listActivitesTypes)
 
  

  res=APMStest.read_one_activity(What)
  print(res) 



  sys.exit()

if __name__=='__main__':
  rc=main()
  sys.exit(rc)
