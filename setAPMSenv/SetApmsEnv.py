import logging
import re
import os
import configparser
import ast


class SetApmsEnv():

    def __init__(self):

        self.set_logger()
        self.setup_environment_dict

    def set_logger(self):
        log_format = "%(funcName)-12s %(asctime)-24s %(process)d %(levelname)-6s %(message)s"
        logging.basicConfig(format=log_format, level=logging.INFO)
        self.myLogger = logging.getLogger()

    @property
    def setup_environment_dict(self):

        env_dict = {}
        config = configparser.ConfigParser()
        script_path = os.getcwd()
        config_file = script_path + '/config/setup.cfg'
        self.myLogger.debug(config_file)
        config.read(config_file)
        env_dict["DATABASE"] = config.get('NAME', 'DATABASE')
        env_dict["LOCATION"] = config.get('DB', 'location')
        env_dict["WSDL"] = config.get('DB', 'wsdl')
        env_dict["PROJECT_ID"] = config.get('Project', 'PROJECT_ID')
        env_dict["PROJECT"] = config.get('Project', 'PROJECT')
        env_dict["USERID"] = config.get('DB', 'userid')
        env_dict["ALPIDEB-CHIP"] = config.get('Object', 'ALPIDEB-Chip')
        env_dict["ALPIDEB-TRAY"] = config.get('Object', 'ALPIDEB-Tray')
        env_dict["LADDER-2"] = config.get('Object', 'LADDER-2')
        env_dict["LADDER-3"] = config.get('Object', 'LADDER-3')
        env_dict["LADDER-4"] = config.get('Object', 'LADDER-4')
        env_dict["LADDER-5"] = config.get('Object', 'LADDER-5')
        env_dict["FPC-2"] = config.get('Object', 'FPC-2')
        env_dict["FPC-3"] = config.get('Object', 'FPC-3')
        env_dict["FPC-4"] = config.get('Object', 'FPC-4')
        env_dict["FPC-5"] = config.get('Object', 'FPC-5')
        env_dict["FPC-SOLD-3"] = config.get('Activity', "FPC-SOLD-3")
        env_dict["FPC-CHK-RAW-VISU-3"] = config.get('Activity', "FPC-CHK-RAW-VISU-3")
        env_dict["FPC-CHK-RAW-ELEC-3"] = config.get('Activity', "FPC-CHK-RAW-ELEC-3")
        env_dict["FPC-CHK-RAW-METR-3"] = config.get('Activity', "FPC-CHK-RAW-METR-3")
        env_dict["FPC-CHK-EQU-VISU-3"] = config.get('Activity', "FPC-CHK-EQU-VISU-3")
        env_dict["FPC-CHK-EQU-ELEC-3"] = config.get('Activity', "FPC-CHK-EQU-ELEC-3")
        env_dict["FPC-CHK-EQU-METR-3"] = config.get('Activity', "FPC-CHK-EQU-METR-3")
        env_dict["FPC-CHK-EQU-CLEA-3"] = config.get('Activity', "FPC-CHK-EQU-CLEA-3")
        env_dict["LADDER-GLUE-3"] = config.get('Activity', "LADDER-GLUE-3")
        env_dict["LADDER-GLUE-3"] = config.get('Activity', "LADDER-GLUE-3")
        env_dict["LADDER-COND-3"] = config.get('Activity', "LADDER-COND-3")
        env_dict["LADDER-BOND-3"] = config.get('Activity', "LADDER-BOND-3")
        env_dict["LADDER-CHK-VISU-3"] = config.get('Activity', "LADDER-CHK-VISU-3")
        env_dict["LADDER-CHK-VISBON-3"] = config.get('Activity', "LADDER-CHK-VISBON-3")
        env_dict["LADDER-CHK-SMOKE-3"] = config.get('Activity', "LADDER-CHK-SMOKE-3")
        env_dict["LADDER-CHK-ELEC-3"] = config.get('Activity', "LADDER-CHK-ELEC-3")
        env_dict["LADDER-CHK-FUNC-3"] = config.get('Activity', "LADDER-CHK-FUNC-3")
        env_dict["TYP-ACT-LAD-GLU-IN-CHIP-3"] = config.get('TypeInActivity', "TYP-ACT-LAD-GLU-IN-CHIP-3")
        env_dict["TYP-ACT-LAD-GLU-IN-FPC-3"] = config.get('TypeInActivity', "TYP-ACT-LAD-GLU-IN-FPC-3")
        env_dict["TYP-ACT-LAD-GLU-OUT-3"] = config.get('TypeInActivity', "TYP-ACT-LAD-GLU-OUT-3")
        env_dict["TYP-ACT-LAD-CHK-VISU-IN-3"] = config.get('TypeInActivity', "TYP-ACT-LAD-CHK-VISU-IN-3")
        env_dict["TYP-ACT-LAD-CHK-VISU-OUT-3"] = config.get('TypeInActivity', "TYP-ACT-LAD-CHK-VISU-OUT-3")
        env_dict["CERN"] = config.get('Location', 'CERN')
        env_dict["OPEN"] = config.get('Status', 'OPEN')
        env_dict["CLOSED"] = config.get('Status', 'CLOSED')
        env_dict["GLUED_OK"] = config.get('Result', 'GLUED_OK')
        env_dict["GLUED_NOK"] = config.get('Result', 'GLUED_NOK')
        env_dict["VISU_INSP_OK"] = config.get('Result', 'VISU_INSP_OK')
        env_dict["VISU_INSP_NOK"] = config.get('Result', 'VISU_INSP_NOK')
        env_dict["BOND_INSP_OK"] = config.get('Result', 'BOND_INSP_OK')
        env_dict["BOND_INSP_NOK"] = config.get('Result', 'BOND_INSP_NOK')

        # New dictionary definition, which should not cause problem when merging. Will be rewritted after merge
        env_dict["COM-FPC-CHK-EQU-CLEA-3-IN"] = config.get("TypeInActivity", "COM-FPC-CHK-EQU-CLEA-3-IN")
        env_dict["COM-FPC-CHK-EQU-CLEA-3-OUT"] = config.get("TypeInActivity", "COM-FPC-CHK-EQU-CLEA-3-OUT")
        env_dict["COM-FPC-CHK-EQU-ELEC-3-IN"] = config.get("TypeInActivity", "COM-FPC-CHK-EQU-ELEC-3-IN")
        env_dict["COM-FPC-CHK-EQU-ELEC-3-OUT"] = config.get("TypeInActivity", "COM-FPC-CHK-EQU-ELEC-3-OUT")
        env_dict["COM-FPC-CHK-EQU-METR-3-IN"] = config.get("TypeInActivity", "COM-FPC-CHK-EQU-METR-3-IN")
        env_dict["COM-FPC-CHK-EQU-METR-3-OUT"] = config.get("TypeInActivity", "COM-FPC-CHK-EQU-METR-3-OUT")
        env_dict["COM-FPC-CHK-EQU-VISU-3-IN"] = config.get("TypeInActivity", "COM-FPC-CHK-EQU-VISU-3-IN")
        env_dict["COM-FPC-CHK-EQU-VISU-3-OUT"] = config.get("TypeInActivity", "COM-FPC-CHK-EQU-VISU-3-OUT")
        env_dict["COM-FPC-CHK-RAW-ELEC-3-IN"] = config.get("TypeInActivity", "COM-FPC-CHK-RAW-ELEC-3-IN")
        env_dict["COM-FPC-CHK-RAW-ELEC-3-OUT"] = config.get("TypeInActivity", "COM-FPC-CHK-RAW-ELEC-3-OUT")
        env_dict["COM-FPC-CHK-RAW-METR-3-IN"] = config.get("TypeInActivity", "COM-FPC-CHK-RAW-METR-3-IN")
        env_dict["COM-FPC-CHK-RAW-METR-3-OUT"] = config.get("TypeInActivity", "COM-FPC-CHK-RAW-METR-3-OUT")
        env_dict["COM-FPC-CHK-RAW-VISU-3-IN"] = config.get("TypeInActivity", "COM-FPC-CHK-RAW-VISU-3-IN")
        env_dict["COM-FPC-CHK-RAW-VISU-3-OUT"] = config.get("TypeInActivity", "COM-FPC-CHK-RAW-VISU-3-OUT")
        env_dict["COM-FPC-SOLD-3-IN"] = config.get("TypeInActivity", "COM-FPC-SOLD-3-IN")
        env_dict["COM-FPC-SOLD-3-OUT"] = config.get("TypeInActivity", "COM-FPC-SOLD-3-OUT")
        env_dict["COM-LADDER-CHK-ELEC-3-IN"] = config.get("TypeInActivity", "COM-LADDER-CHK-ELEC-3-IN")
        env_dict["COM-LADDER-CHK-ELEC-3-OUT"] = config.get("TypeInActivity", "COM-LADDER-CHK-ELEC-3-OUT")
        env_dict["COM-LADDER-CHK-FUNC-3-IN"] = config.get("TypeInActivity", "COM-LADDER-CHK-FUNC-3-IN")
        env_dict["COM-LADDER-CHK-FUNC-3-OUT"] = config.get("TypeInActivity", "COM-LADDER-CHK-FUNC-3-OUT")
        env_dict["COM-LADDER-CHK-SMOKE-3-IN"] = config.get("TypeInActivity", "COM-LADDER-CHK-SMOKE-3-IN")
        env_dict["COM-LADDER-CHK-SMOKE-3-OUT"] = config.get("TypeInActivity", "COM-LADDER-CHK-SMOKE-3-OUT")
        env_dict["COM-LADDER-CHK-VISBON-3-IN"] = config.get("TypeInActivity", "COM-LADDER-CHK-VISBON-3-IN")
        env_dict["COM-LADDER-CHK-VISBON-3-OUT"] = config.get("TypeInActivity", "COM-LADDER-CHK-VISBON-3-OUT")
        env_dict["COM-LADDER-CHK-VISU-3-IN"] = config.get("TypeInActivity", "COM-LADDER-CHK-VISU-3-IN")
        env_dict["COM-LADDER-CHK-VISU-3-OUT"] = config.get("TypeInActivity", "COM-LADDER-CHK-VISU-3-OUT")
        env_dict["COM-LADDER-BOND-3-IN"] = config.get("TypeInActivity", "COM-LADDER-BOND-3-IN")
        env_dict["COM-LADDER-BOND-3-OUT"] = config.get("TypeInActivity", "COM-LADDER-BOND-3-OUT")
        env_dict["COM-LADDER-COND-3-IN"] = config.get("TypeInActivity", "COM-LADDER-COND-3-IN")
        env_dict["COM-LADDER-COND-3-OUT"] = config.get("TypeInActivity", "COM-LADDER-COND-3-OUT")
        env_dict["COM-LADDER-GLUE-3-IN-FPC"] = config.get("TypeInActivity", "COM-LADDER-GLUE-3-IN-FPC")
        env_dict["COM-LADDER-GLUE-3-IN-CHIP"] = config.get("TypeInActivity", "COM-LADDER-GLUE-3-IN-CHIP")
        env_dict["COM-LADDER-GLUE-3-OUT"] = config.get("TypeInActivity", "COM-LADDER-GLUE-3-OUT")

        env_dict["LOC-FPC-CHK-EQU-CLEA-3-CERN"] = config.get("Location", "LOC-FPC-CHK-EQU-CLEA-3-CERN")
        env_dict["LOC-FPC-CHK-EQU-CLEA-3-IPNL"] = config.get("Location", "LOC-FPC-CHK-EQU-CLEA-3-IPNL")
        env_dict["LOC-FPC-CHK-EQU-ELEC-3-CERN"] = config.get("Location", "LOC-FPC-CHK-EQU-ELEC-3-CERN")
        env_dict["LOC-FPC-CHK-EQU-ELEC-3-IPNL"] = config.get("Location", "LOC-FPC-CHK-EQU-ELEC-3-IPNL")
        env_dict["LOC-FPC-CHK-EQU-METR-3-CERN"] = config.get("Location", "LOC-FPC-CHK-EQU-METR-3-CERN")
        env_dict["LOC-FPC-CHK-EQU-METR-3-IPNL"] = config.get("Location", "LOC-FPC-CHK-EQU-METR-3-IPNL")
        env_dict["LOC-FPC-CHK-EQU-VISU-3-CERN"] = config.get("Location", "LOC-FPC-CHK-EQU-VISU-3-CERN")
        env_dict["LOC-FPC-CHK-EQU-VISU-3-IPNL"] = config.get("Location", "LOC-FPC-CHK-EQU-VISU-3-IPNL")
        env_dict["LOC-FPC-CHK-RAW-ELEC-3-CERN"] = config.get("Location", "LOC-FPC-CHK-RAW-ELEC-3-CERN")
        env_dict["LOC-FPC-CHK-RAW-ELEC-3-IPNL"] = config.get("Location", "LOC-FPC-CHK-RAW-ELEC-3-IPNL")
        env_dict["LOC-FPC-CHK-RAW-METR-3-CERN"] = config.get("Location", "LOC-FPC-CHK-RAW-METR-3-CERN")
        env_dict["LOC-FPC-CHK-RAW-METR-3-IPNL"] = config.get("Location", "LOC-FPC-CHK-RAW-METR-3-IPNL")
        env_dict["LOC-FPC-CHK-RAW-VISU-3-CERN"] = config.get("Location", "LOC-FPC-CHK-RAW-VISU-3-CERN")
        env_dict["LOC-FPC-CHK-RAW-VISU-3-IPNL"] = config.get("Location", "LOC-FPC-CHK-RAW-VISU-3-IPNL")
        env_dict["LOC-FPC-SOLD-3-CERN"] = config.get("Location", "LOC-FPC-SOLD-3-CERN")
        env_dict["LOC-FPC-SOLD-3-IPNL"] = config.get("Location", "LOC-FPC-SOLD-3-IPNL")
        env_dict["LOC-LADDER-CHK-ELEC-3-CERN"] = config.get("Location", "LOC-LADDER-CHK-ELEC-3-CERN")
        env_dict["LOC-LADDER-CHK-ELEC-3-IPNL"] = config.get("Location", "LOC-LADDER-CHK-ELEC-3-IPNL")
        env_dict["LOC-LADDER-CHK-FUNC-3-CERN"] = config.get("Location", "LOC-LADDER-CHK-FUNC-3-CERN")
        env_dict["LOC-LADDER-CHK-FUNC-3-IPNL"] = config.get("Location", "LOC-LADDER-CHK-FUNC-3-IPNL")
        env_dict["LOC-LADDER-CHK-SMOKE-3-CERN"] = config.get("Location", "LOC-LADDER-CHK-SMOKE-3-CERN")
        env_dict["LOC-LADDER-CHK-SMOKE-3-IPNL"] = config.get("Location", "LOC-LADDER-CHK-SMOKE-3-IPNL")
        env_dict["LOC-LADDER-CHK-VISBON-3-CERN"] = config.get("Location", "LOC-LADDER-CHK-VISBON-3-CERN")
        env_dict["LOC-LADDER-CHK-VISBON-3-IPNL"] = config.get("Location", "LOC-LADDER-CHK-VISBON-3-IPNL")
        env_dict["LOC-LADDER-CHK-VISU-3-CERN"] = config.get("Location", "LOC-LADDER-CHK-VISU-3-CERN")
        env_dict["LOC-LADDER-CHK-VISU-3-IPNL"] = config.get("Location", "LOC-LADDER-CHK-VISU-3-IPNL")
        env_dict["LOC-LADDER-BOND-3-CERN"] = config.get("Location", "LOC-LADDER-BOND-3-CERN")
        env_dict["LOC-LADDER-BOND-3-IPNL"] = config.get("Location", "LOC-LADDER-BOND-3-IPNL")
        env_dict["LOC-LADDER-COND-3-CERN"] = config.get("Location", "LOC-LADDER-COND-3-CERN")
        env_dict["LOC-LADDER-COND-3-IPNL"] = config.get("Location", "LOC-LADDER-COND-3-IPNL")
        env_dict["LOC-LADDER-GLUE-3-CERN"] = config.get("Location", "LOC-LADDER-GLUE-3-CERN")
        env_dict["LOC-LADDER-GLUE-3-IPNL"] = config.get("Location", "LOC-LADDER-GLUE-3-IPNL")

        env_dict["STATUS-OPEN"] = config.get('Status', 'STATUS-OPEN')
        env_dict["STATUS-CLOSED"] = config.get('Status', 'STATUS-CLOSED')

        env_dict["RES-FPC-CHK-EQU-CLEA-3-OK"] = config.get("Result", "RES-FPC-CHK-EQU-CLEA-3-OK")
        env_dict["RES-FPC-CHK-EQU-CLEA-3-NOK"] = config.get("Result", "RES-FPC-CHK-EQU-CLEA-3-NOK")
        env_dict["RES-FPC-CHK-EQU-ELEC-3-OK"] = config.get("Result", "RES-FPC-CHK-EQU-ELEC-3-OK")
        env_dict["RES-FPC-CHK-EQU-ELEC-3-NOK"] = config.get("Result", "RES-FPC-CHK-EQU-ELEC-3-NOK")
        env_dict["RES-FPC-CHK-EQU-METR-3-OK"] = config.get("Result", "RES-FPC-CHK-EQU-METR-3-OK")
        env_dict["RES-FPC-CHK-EQU-METR-3-NOK"] = config.get("Result", "RES-FPC-CHK-EQU-METR-3-NOK")
        env_dict["RES-FPC-CHK-EQU-VISU-3-OK"] = config.get("Result", "RES-FPC-CHK-EQU-VISU-3-OK")
        env_dict["RES-FPC-CHK-EQU-VISU-3-NOK"] = config.get("Result", "RES-FPC-CHK-EQU-VISU-3-NOK")
        env_dict["RES-FPC-CHK-RAW-ELEC-3-OK"] = config.get("Result", "RES-FPC-CHK-RAW-ELEC-3-OK")
        env_dict["RES-FPC-CHK-RAW-ELEC-3-NOK"] = config.get("Result", "RES-FPC-CHK-RAW-ELEC-3-NOK")
        env_dict["RES-FPC-CHK-RAW-METR-3-OK"] = config.get("Result", "RES-FPC-CHK-RAW-METR-3-OK")
        env_dict["RES-FPC-CHK-RAW-METR-3-NOK"] = config.get("Result", "RES-FPC-CHK-RAW-METR-3-NOK")
        env_dict["RES-FPC-CHK-RAW-VISU-3-OK"] = config.get("Result", "RES-FPC-CHK-RAW-VISU-3-OK")
        env_dict["RES-FPC-CHK-RAW-VISU-3-NOK"] = config.get("Result", "RES-FPC-CHK-RAW-VISU-3-NOK")
        env_dict["RES-FPC-SOLD-3-OK"] = config.get("Result", "RES-FPC-SOLD-3-OK")
        env_dict["RES-FPC-SOLD-3-NOK"] = config.get("Result", "RES-FPC-SOLD-3-NOK")
        env_dict["RES-LADDER-CHK-ELEC-3-OK"] = config.get("Result", "RES-LADDER-CHK-ELEC-3-OK")
        env_dict["RES-LADDER-CHK-ELEC-3-NOK"] = config.get("Result", "RES-LADDER-CHK-ELEC-3-NOK")
        env_dict["RES-LADDER-CHK-FUNC-3-OK"] = config.get("Result", "RES-LADDER-CHK-FUNC-3-OK")
        env_dict["RES-LADDER-CHK-FUNC-3-NOK"] = config.get("Result", "RES-LADDER-CHK-FUNC-3-NOK")
        env_dict["RES-LADDER-CHK-SMOKE-3-OK"] = config.get("Result", "RES-LADDER-CHK-SMOKE-3-OK")
        env_dict["RES-LADDER-CHK-SMOKE-3-NOK"] = config.get("Result", "RES-LADDER-CHK-SMOKE-3-NOK")
        env_dict["RES-LADDER-CHK-VISBON-3-OK"] = config.get("Result", "RES-LADDER-CHK-VISBON-3-OK")
        env_dict["RES-LADDER-CHK-VISBON-3-NOK"] = config.get("Result", "RES-LADDER-CHK-VISBON-3-NOK")
        env_dict["RES-LADDER-CHK-VISU-3-OK"] = config.get("Result", "RES-LADDER-CHK-VISU-3-OK")
        env_dict["RES-LADDER-CHK-VISU-3-NOK"] = config.get("Result", "RES-LADDER-CHK-VISU-3-NOK")
        env_dict["RES-LADDER-BOND-3-OK"] = config.get("Result", "RES-LADDER-BOND-3-OK")
        env_dict["RES-LADDER-BOND-3-NOK"] = config.get("Result", "RES-LADDER-BOND-3-NOK")
        env_dict["RES-LADDER-COND-3-OK"] = config.get("Result", "RES-LADDER-COND-3-OK")
        env_dict["RES-LADDER-COND-3-NOK"] = config.get("Result", "RES-LADDER-COND-3-NOK")
        env_dict["RES-LADDER-GLUE-3-OK"] = config.get("Result", "RES-LADDER-GLUE-3-OK")
        env_dict["RES-LADDER-GLUE-3-NOK"] = config.get("Result", "RES-LADDER-GLUE-3-NOK")

        env_dict["PAR-FPC-CHK-EQU-CLEA-3-GOL"] = config.get("Parameter", "PAR-FPC-CHK-EQU-CLEA-3-GOL")
        env_dict["PAR-FPC-CHK-EQU-CLEA-3-SIL"] = config.get("Parameter", "PAR-FPC-CHK-EQU-CLEA-3-SIL")
        env_dict["PAR-FPC-CHK-EQU-ELEC-3-GOL"] = config.get("Parameter", "PAR-FPC-CHK-EQU-ELEC-3-GOL")
        env_dict["PAR-FPC-CHK-EQU-ELEC-3-SIL"] = config.get("Parameter", "PAR-FPC-CHK-EQU-ELEC-3-SIL")
        env_dict["PAR-FPC-CHK-EQU-METR-3-GOL"] = config.get("Parameter", "PAR-FPC-CHK-EQU-METR-3-GOL")
        env_dict["PAR-FPC-CHK-EQU-METR-3-SIL"] = config.get("Parameter", "PAR-FPC-CHK-EQU-METR-3-SIL")
        env_dict["PAR-FPC-CHK-EQU-VISU-3-GOL"] = config.get("Parameter", "PAR-FPC-CHK-EQU-VISU-3-GOL")
        env_dict["PAR-FPC-CHK-EQU-VISU-3-SIL"] = config.get("Parameter", "PAR-FPC-CHK-EQU-VISU-3-SIL")
        env_dict["PAR-FPC-CHK-RAW-ELEC-3-GOL"] = config.get("Parameter", "PAR-FPC-CHK-RAW-ELEC-3-GOL")
        env_dict["PAR-FPC-CHK-RAW-ELEC-3-SIL"] = config.get("Parameter", "PAR-FPC-CHK-RAW-ELEC-3-SIL")
        env_dict["PAR-FPC-CHK-RAW-METR-3-GOL"] = config.get("Parameter", "PAR-FPC-CHK-RAW-METR-3-GOL")
        env_dict["PAR-FPC-CHK-RAW-METR-3-SIL"] = config.get("Parameter", "PAR-FPC-CHK-RAW-METR-3-SIL")
        env_dict["PAR-FPC-CHK-RAW-VISU-3-GOL"] = config.get("Parameter", "PAR-FPC-CHK-RAW-VISU-3-GOL")
        env_dict["PAR-FPC-CHK-RAW-VISU-3-SIL"] = config.get("Parameter", "PAR-FPC-CHK-RAW-VISU-3-SIL")
        env_dict["PAR-FPC-SOLD-3-GOL"] = config.get("Parameter", "PAR-FPC-SOLD-3-GOL")
        env_dict["PAR-FPC-SOLD-3-SIL"] = config.get("Parameter", "PAR-FPC-SOLD-3-SIL")
        env_dict["PAR-LADDER-CHK-ELEC-3-GOL"] = config.get("Parameter", "PAR-LADDER-CHK-ELEC-3-GOL")
        env_dict["PAR-LADDER-CHK-ELEC-3-SIL"] = config.get("Parameter", "PAR-LADDER-CHK-ELEC-3-SIL")
        env_dict["PAR-LADDER-CHK-FUNC-3-GOL"] = config.get("Parameter", "PAR-LADDER-CHK-FUNC-3-GOL")
        env_dict["PAR-LADDER-CHK-FUNC-3-SIL"] = config.get("Parameter", "PAR-LADDER-CHK-FUNC-3-SIL")
        env_dict["PAR-LADDER-CHK-SMOKE-3-GOL"] = config.get("Parameter", "PAR-LADDER-CHK-SMOKE-3-GOL")
        env_dict["PAR-LADDER-CHK-SMOKE-3-SIL"] = config.get("Parameter", "PAR-LADDER-CHK-SMOKE-3-SIL")
        env_dict["PAR-LADDER-CHK-VISBON-3-GOL"] = config.get("Parameter", "PAR-LADDER-CHK-VISBON-3-GOL")
        env_dict["PAR-LADDER-CHK-VISBON-3-SIL"] = config.get("Parameter", "PAR-LADDER-CHK-VISBON-3-SIL")
        env_dict["PAR-LADDER-CHK-VISU-3-GOL"] = config.get("Parameter", "PAR-LADDER-CHK-VISU-3-GOL")
        env_dict["PAR-LADDER-CHK-VISU-3-SIL"] = config.get("Parameter", "PAR-LADDER-CHK-VISU-3-SIL")
        env_dict["PAR-LADDER-BOND-3-GOL"] = config.get("Parameter", "PAR-LADDER-BOND-3-GOL")
        env_dict["PAR-LADDER-BOND-3-SIL"] = config.get("Parameter", "PAR-LADDER-BOND-3-SIL")
        env_dict["PAR-LADDER-COND-3-GOL"] = config.get("Parameter", "PAR-LADDER-COND-3-GOL")
        env_dict["PAR-LADDER-COND-3-SIL"] = config.get("Parameter", "PAR-LADDER-COND-3-SIL")
        env_dict["PAR-LADDER-GLUE-3-GOL"] = config.get("Parameter", "PAR-LADDER-GLUE-3-GOL")
        env_dict["PAR-LADDER-GLUE-3-SIL"] = config.get("Parameter", "PAR-LADDER-GLUE-3-SIL")
        env_dict["PAR-FPC-CHK-EQU-CLEA-3-BRO"] = config.get("Parameter", "PAR-FPC-CHK-EQU-CLEA-3-BRO")
        env_dict["PAR-FPC-CHK-EQU-CLEA-3-BAD"] = config.get("Parameter", "PAR-FPC-CHK-EQU-CLEA-3-BAD")
        env_dict["PAR-FPC-CHK-EQU-ELEC-3-BRO"] = config.get("Parameter", "PAR-FPC-CHK-EQU-ELEC-3-BRO")
        env_dict["PAR-FPC-CHK-EQU-ELEC-3-BAD"] = config.get("Parameter", "PAR-FPC-CHK-EQU-ELEC-3-BAD")
        env_dict["PAR-FPC-CHK-EQU-METR-3-BRO"] = config.get("Parameter", "PAR-FPC-CHK-EQU-METR-3-BRO")
        env_dict["PAR-FPC-CHK-EQU-METR-3-BAD"] = config.get("Parameter", "PAR-FPC-CHK-EQU-METR-3-BAD")
        env_dict["PAR-FPC-CHK-EQU-VISU-3-BRO"] = config.get("Parameter", "PAR-FPC-CHK-EQU-VISU-3-BRO")
        env_dict["PAR-FPC-CHK-EQU-VISU-3-BAD"] = config.get("Parameter", "PAR-FPC-CHK-EQU-VISU-3-BAD")
        env_dict["PAR-FPC-CHK-RAW-ELEC-3-BRO"] = config.get("Parameter", "PAR-FPC-CHK-RAW-ELEC-3-BRO")
        env_dict["PAR-FPC-CHK-RAW-ELEC-3-BAD"] = config.get("Parameter", "PAR-FPC-CHK-RAW-ELEC-3-BAD")
        env_dict["PAR-FPC-CHK-RAW-METR-3-BRO"] = config.get("Parameter", "PAR-FPC-CHK-RAW-METR-3-BRO")
        env_dict["PAR-FPC-CHK-RAW-METR-3-BAD"] = config.get("Parameter", "PAR-FPC-CHK-RAW-METR-3-BAD")
        env_dict["PAR-FPC-CHK-RAW-VISU-3-BRO"] = config.get("Parameter", "PAR-FPC-CHK-RAW-VISU-3-BRO")
        env_dict["PAR-FPC-CHK-RAW-VISU-3-BAD"] = config.get("Parameter", "PAR-FPC-CHK-RAW-VISU-3-BAD")
        env_dict["PAR-FPC-SOLD-3-BRO"] = config.get("Parameter", "PAR-FPC-SOLD-3-BRO")
        env_dict["PAR-FPC-SOLD-3-BAD"] = config.get("Parameter", "PAR-FPC-SOLD-3-BAD")
        env_dict["PAR-LADDER-CHK-ELEC-3-BRO"] = config.get("Parameter", "PAR-LADDER-CHK-ELEC-3-BRO")
        env_dict["PAR-LADDER-CHK-ELEC-3-BAD"] = config.get("Parameter", "PAR-LADDER-CHK-ELEC-3-BAD")
        env_dict["PAR-LADDER-CHK-FUNC-3-BRO"] = config.get("Parameter", "PAR-LADDER-CHK-FUNC-3-BRO")
        env_dict["PAR-LADDER-CHK-FUNC-3-BAD"] = config.get("Parameter", "PAR-LADDER-CHK-FUNC-3-BAD")
        env_dict["PAR-LADDER-CHK-SMOKE-3-BRO"] = config.get("Parameter", "PAR-LADDER-CHK-SMOKE-3-BRO")
        env_dict["PAR-LADDER-CHK-SMOKE-3-BAD"] = config.get("Parameter", "PAR-LADDER-CHK-SMOKE-3-BAD")
        env_dict["PAR-LADDER-CHK-VISBON-3-BRO"] = config.get("Parameter", "PAR-LADDER-CHK-VISBON-3-BRO")
        env_dict["PAR-LADDER-CHK-VISBON-3-BAD"] = config.get("Parameter", "PAR-LADDER-CHK-VISBON-3-BAD")
        env_dict["PAR-LADDER-CHK-VISU-3-BRO"] = config.get("Parameter", "PAR-LADDER-CHK-VISU-3-BRO")
        env_dict["PAR-LADDER-CHK-VISU-3-BAD"] = config.get("Parameter", "PAR-LADDER-CHK-VISU-3-BAD")
        env_dict["PAR-LADDER-BOND-3-BRO"] = config.get("Parameter", "PAR-LADDER-BOND-3-BRO")
        env_dict["PAR-LADDER-BOND-3-BAD"] = config.get("Parameter", "PAR-LADDER-BOND-3-BAD")
        env_dict["PAR-LADDER-COND-3-BRO"] = config.get("Parameter", "PAR-LADDER-COND-3-BRO")
        env_dict["PAR-LADDER-COND-3-BAD"] = config.get("Parameter", "PAR-LADDER-COND-3-BAD")
        env_dict["PAR-LADDER-GLUE-3-BRO"] = config.get("Parameter", "PAR-LADDER-GLUE-3-BRO")
        env_dict["PAR-LADDER-GLUE-3-BAD"] = config.get("Parameter", "PAR-LADDER-GLUE-3-BAD")

        return env_dict

    def set_status_dict(self, activity_type):
        import ActivityTypeInfo
        ActivityTypeInfo.main(activity_type)

        fichier = open("DataToAdd/ActivityInformations.txt",
                       "r")  # Chips.txt contains all Chips of one database. This file was create by GetAllChips.py
        liste = fichier.read()  # here type(liste) = 'str'
        liste = ast.literal_eval(liste)  # Change liste type to 'list'
        fichier.close()

        if (liste["Status"] == None):
            res = "Null"
        else:
            liste_res = []
            dico = {}
            les_status = liste["Status"]
            for un in les_status["ActivityStatus"]:
                dico[un["Code"]] = un["ID"]
            liste_res.append(dico)
            res = liste_res
        fichier = open("config/ITSStatus.txt", "w")
        fichier.write(str(res))
        fichier.close()

        return (res)

    def set_location_dict(self, activity_type):
        import ActivityTypeInfo
        ActivityTypeInfo.main(activity_type)

        fichier = open("DataToAdd/ActivityInformations.txt",
                       "r")  # Chips.txt contains all Chips of one database. This file was create by GetAllChips.py
        liste = fichier.read()  # here type(liste) = 'str'
        liste = ast.literal_eval(liste)  # Change liste type to 'list'
        fichier.close()

        if (liste["Location"] == None):
            res = "Null"
        else:
            liste_res = []
            dico = {}
            les_status = liste["Location"]
            for un in les_status["ActivityTypeLocation"]:
                dico[un["Name"]] = un["ID"]
            liste_res.append(dico)
            res = liste_res
        fichier = open("config/ITSLocation.txt", "w")
        fichier.write(str(res))
        fichier.close()

        return (res)

    def set_atct_dict(self, activity_type):
        import ActivityTypeInfo
        ActivityTypeInfo.main(activity_type)

        fichier = open("DataToAdd/ActivityInformations.txt",
                       "r")  # Chips.txt contains all Chips of one database. This file was create by GetAllChips.py
        liste = fichier.read()  # here type(liste) = 'str'
        liste = ast.literal_eval(liste)  # Change liste type to 'list'
        fichier.close()
        if (liste["ActivityTypeComponentType"] == None):
            res = "Null"
        else:
            liste_res = []
            dico = {}
            les_status = liste["ActivityTypeComponentType"]
            for un in les_status["ActivityTypeComponentTypeFull"]:
                dico[un["Direction"]] = un["ID"]
            liste_res.append(dico)
            res = liste_res
        fichier = open("config/ITSATCT.txt", "w")
        fichier.write(str(res))
        fichier.close()

        return (res)

    def set_material_conformity_dict(self, activity_type):
        import ActivityTypeInfo
        ActivityTypeInfo.main(activity_type)

        fichier = open("DataToAdd/ActivityInformations.txt",
                       "r")  # Chips.txt contains all Chips of one database. This file was create by GetAllChips.py
        liste = fichier.read()  # here type(liste) = 'str'
        liste = ast.literal_eval(liste)  # Change liste type to 'list'
        fichier.close()

        if (liste["MaterialConformity"] == None):
            res = "Null"
        else:
            liste_res = []
            dico = {}
            les_m_conformity = liste["MaterialConformity"]
            for un in les_m_conformity["ActivityTypeMaterialConformity"]:
                # print(un)
                for one in un:
                    if one == "FunctionalStatus":
                        dico[un["FunctionalStatus"]["Name"]] = un["FunctionalStatus"]["ID"]
            liste_res.append(dico)
            res = liste_res

        fichier = open("config/ITSMaterialConformity.txt", "w")
        fichier.write(str(res))
        fichier.close()

        print(res)
        return (res)

    def set_result_dict(self, activity_type):
        import ActivityTypeInfo
        ActivityTypeInfo.main(activity_type)

        fichier = open("DataToAdd/ActivityInformations.txt",
                       "r")  # Chips.txt contains all Chips of one database. This file was create by GetAllChips.py
        liste = fichier.read()  # here type(liste) = 'str'
        liste = ast.literal_eval(liste)  # Change liste type to 'list'
        fichier.close()

        if (liste["Result"] == None):
            res = "Null"
        else:
            liste_res = []
            dico = {}
            les_results = liste["Result"]
            for un in les_results["ActivityTypeResultFull"]:
                dico[un["Name"]] = un["ID"]
            liste_res.append(dico)
            res = liste_res
        fichier = open("config/ITSResults.txt", "w")
        fichier.write(str(res))
        fichier.close()

        return (res)

    def set_parameters_dict(self, activity_type):
        import ActivityTypeInfo
        ActivityTypeInfo.main(activity_type)

        fichier = open("DataToAdd/ActivityInformations.txt",
                       "r")  # Chips.txt contains all Chips of one database. This file was create by GetAllChips.py
        liste = fichier.read()  # here type(liste) = 'str'
        liste = ast.literal_eval(liste)  # Change liste type to 'list'
        fichier.close()

        if (liste["Parameters"] == None):
            res = "Null"
        else:
            liste_res = []
            dico = {}
            les_results = liste["Parameters"]
            for un in les_results["ActivityTypeParameter"]:
                dico[un["Parameter"]["Name"]] = un["Parameter"]["ID"]
            liste_res.append(dico)
            res = liste_res
        fichier = open("config/ITSParameters.txt", "w")
        fichier.write(str(res))
        fichier.close()

        return (res)

    def foo(self, a):
        print(a + 2)
