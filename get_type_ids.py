"""This script is mentioned to obtain type ids of all activities, components, results etc.,
which are needed for setup configuration file.
"""

__author__ = "Roman Lavicka"

# Standard libraries
import glob
import os
import sys
import time

# Special libraries

# Local libraries
from APMSdatabase import APMSdatabase
from setAPMSenv import SetApmsEnv

def main():
    """Main corpse of this script.
    """

    # Setting up database
    environment_setup = SetApmsEnv.SetApmsEnv()
    environment_dict = environment_setup.setup_environment_dict
    db = APMSdatabase.APMSdatabase(
        environment_dict.get("PROJECT"),
        int(environment_dict.get("PROJECT_ID")),
        int(environment_dict.get("USERID")),
        environment_dict.get("WSDL"),
    )

    # Perform action
    clear_type_ids_files()

    get_fpc_activity_type_ids(db)
    get_ladder_activity_type_ids(db)
    get_components_type_ids(db)
    get_activities_status_type_ids(db,1788)

    return "\nScript get_type_ids finished."

def clear_type_ids_files():
    """Delete all files in type_ids
    """

    # Get a list of all the file paths that ends with .txt from in specified directory
    file_list = glob.glob("localData/type_ids/*.txt")

    # Iterate over the list of filepaths & remove each file.
    for file_path in file_list:
        try:
            os.remove(file_path)
        except:
            sys.exit("Error while deleting file : " + str(file_path))

    return 1

def get_activities_status_type_ids(db,activity_of_type):
    """Print to file type IDs of activity status - open/closed. This should be the same for all activities
    """

    status_string = "STATUS-"

    file = open("localData/type_ids/statusActivityTypeIDs.txt", "w")

    # Load activity attributes
    activity = db.activity_type_read_all(activity_of_type)

    # Get all types of statuses
    statuses = activity["Status"]["ActivityStatus"]

    # Search in statuses and get status type you want -> store locally
    for status in statuses:
        if status["Code"].find("OPEN") != -1:
            file.write(str(status_string)+"OPEN="+str(status["ID"])+"\n")
        if status["Code"].find("CLOSED") != -1:
            file.write(str(status_string)+"CLOSED="+str(status["ID"])+"\n")

    file.close()

    return 1

def get_components_type_ids_related_to_activity(db, activity_of_type, generic_name):
    """Print to file type IDs of all input/output components of related activity.
    """

    location_string = "COM-"

    file = open("localData/type_ids/componentsActivityTypeIDs.txt", "a+")

    # Load activity attributes
    activity = db.activity_type_read_all(activity_of_type)

    # Get all types of components
    components = activity["ActivityTypeComponentType"]["ActivityTypeComponentTypeFull"]

    # Search in components and get component type you want -> store locally
    for component in components:
        if component["Direction"].find("out") != -1:
            file.write(str(location_string)+str(generic_name)+"-OUT="+str(component["ID"])+"\n")
        if component["Direction"].find("in") != -1:
            # Difference between assembly and non-assembly activity
            if len(components) == 3:
                if component["Quantity"] == 1:
                    file.write(str(location_string)+str(generic_name)+"-IN-FPC="+str(component["ID"])+"\n")
                else:
                    file.write(str(location_string)+str(generic_name)+"-IN-CHIP="+str(component["ID"])+"\n")
            else:
                file.write(str(location_string) + str(generic_name) + "-IN=" + str(component["ID"]) + "\n")

    file.close()
    return 1

def get_location_type_ids_related_to_activity(db, activity_of_type, generic_name):
    """Print to file type IDs of all locations of related activity.
    """

    location_string = "LOC-"

    file = open("localData/type_ids/locationActivityTypeIDs.txt", "a+")

    # Load activity attributes
    activity = db.activity_type_read_all(activity_of_type)

    # Get all types of locations
    locations = activity["Location"]["ActivityTypeLocation"]

    # Search in locations and get location type you want -> store locally
    for location in locations:
        if location["Name"].find("CERN") != -1:
            file.write(str(location_string)+str(generic_name)+"-CERN="+str(location["ID"])+"\n")
        if location["Name"].find("Lyon") != -1:
            file.write(str(location_string)+str(generic_name)+"-IPNL="+str(location["ID"])+"\n")

    file.close()
    return 1

def get_parameter_type_ids_related_to_activity(db, activity_of_type, generic_name):
    """Print to file type IDs of all parameters of related activity.
    """

    location_string = "PAR-"

    file = open("localData/type_ids/parameterActivityTypeIDs.txt", "a+")

    # Load activity attributes
    activity = db.activity_type_read_all(activity_of_type)

    # Get all types of parameters
    parameters = activity["Parameters"]["ActivityTypeParameter"]

    # Search in parameters and get parameter type you want -> store locally
    for parameter in parameters:
        if parameter["Parameter"]["Name"] == "Gold":
            file.write(str(location_string)+str(generic_name)+"-GOL="+str(parameter["ID"])+"\n")
        if parameter["Parameter"]["Name"] == "Silver":
            file.write(str(location_string)+str(generic_name)+"-SIL="+str(parameter["ID"])+"\n")
        if parameter["Parameter"]["Name"] == "Bronze":
            file.write(str(location_string)+str(generic_name)+"-BRO="+str(parameter["ID"])+"\n")
        if parameter["Parameter"]["Name"] == "Bad":
            file.write(str(location_string)+str(generic_name)+"-BAD="+str(parameter["ID"])+"\n")

    file.close()
    return 1


def get_result_type_ids_related_to_activity(db, activity_of_type, generic_name):
    """Print to file type IDs of all results of related activity.
    """

    result_string = "RES-"

    file = open("localData/type_ids/resultActivityTypeIDs.txt", "a+")

    # Load activity attributes
    activity = db.activity_type_read_all(activity_of_type)

    # Get all types of results
    results = activity["Result"]["ActivityTypeResultFull"]

    # Search in results and get result type you want -> store locally
    for result in results:
        if result["Name"].find("_OK") != -1:
            if result["Name"].find("NOT_OK") != -1:  # Fix inconsistent naming in db
                file.write(str(result_string)+str(generic_name)+"-NOK="+str(result["ID"])+"\n")
            else:
                file.write(str(result_string)+str(generic_name)+"-OK="+str(result["ID"])+"\n")
        if result["Name"].find("_NOK") != -1:
            file.write(str(result_string)+str(generic_name)+"-NOK="+str(result["ID"])+"\n")

    file.close()
    return 1

def get_more_type_ids_related_to_activity(db, activity_db_id, generic_name):
    """Function to perform multiple type_ids getters
    """
    get_components_type_ids_related_to_activity(db, activity_db_id, generic_name)
    get_parameter_type_ids_related_to_activity(db, activity_db_id, generic_name)
    get_location_type_ids_related_to_activity(db, activity_db_id, generic_name)
    get_result_type_ids_related_to_activity(db, activity_db_id, generic_name)

    return 1

def get_fpc_activity_type_ids(db):
    """Print to file type IDs of fpc activities in DB.
    Note, that order in the output file depends on the order of activities in database (and not this local order).
    """

    file = open("localData/type_ids/fpcActivityTypeIDs.txt", "w")
    # file.write("[Activity]\n")

    list_of_activities = db.list_activities_types()

    for n_chips in range(2,6):
        for activity in list_of_activities:
            if activity["Name"].find("New FPC RAW CHK VISU "+str(n_chips)) != -1:
                generic_name = "FPC-CHK-RAW-VISU-"+str(n_chips)
                file.write(str(generic_name)+"="+str(activity["ID"])+"\n")
                get_more_type_ids_related_to_activity(db, activity["ID"], generic_name)
            if activity["Name"].find("New FPC RAW CHK ELEC "+str(n_chips)) != -1:
                generic_name = "FPC-CHK-RAW-ELEC-"+str(n_chips)
                file.write(str(generic_name)+"="+str(activity["ID"])+"\n")
                get_more_type_ids_related_to_activity(db, activity["ID"], generic_name)
            if activity["Name"].find("New FPC RAW CHK METR "+str(n_chips)) != -1:
                generic_name = "FPC-CHK-RAW-METR-"+str(n_chips)
                file.write(str(generic_name)+"="+str(activity["ID"])+"\n")
                get_more_type_ids_related_to_activity(db, activity["ID"], generic_name)

            if activity["Name"].find("New FPC SOLD "+str(n_chips)) != -1:
                generic_name = "FPC-SOLD-"+str(n_chips)
                file.write(str(generic_name)+"="+str(activity["ID"])+"\n")
                get_more_type_ids_related_to_activity(db, activity["ID"], generic_name)

            if activity["Name"].find("New FPC EQU CHK ELEC "+str(n_chips)) != -1:
                generic_name = "FPC-CHK-EQU-ELEC-"+str(n_chips)
                file.write(str(generic_name)+"="+str(activity["ID"])+"\n")
                get_more_type_ids_related_to_activity(db, activity["ID"], generic_name)
            if activity["Name"].find("New FPC EQU CHK METR "+str(n_chips)) != -1:
                generic_name = "FPC-CHK-EQU-METR-"+str(n_chips)
                file.write(str(generic_name)+"="+str(activity["ID"])+"\n")
                get_more_type_ids_related_to_activity(db, activity["ID"], generic_name)
            if activity["Name"].find("New FPC EQU CHK CLEAN "+str(n_chips)) != -1:
                generic_name = "FPC-CHK-EQU-CLEA-"+str(n_chips)
                file.write(str(generic_name)+"="+str(activity["ID"])+"\n")
                get_more_type_ids_related_to_activity(db, activity["ID"], generic_name)
            if activity["Name"].find("New FPC EQU CHK VISU "+str(n_chips)) != -1:
                generic_name = "FPC-CHK-EQU-VISU-"+str(n_chips)
                file.write(str(generic_name)+"="+str(activity["ID"])+"\n")
                get_more_type_ids_related_to_activity(db, activity["ID"], generic_name)

    file.close()
    return 1


def get_ladder_activity_type_ids(db):
    """Print to file type IDs of all ladder activities in DB.
    For each activity gets all sub type ids of its features (ActivityStatus, ActivityComponent etc...)
    Note, that order in the output file depends on the order of activities in database (and not this local order).
    """

    file = open("localData/type_ids/ladderActivityTypeIDs.txt", "w")
    # file.write("[Activity]\n")

    list_of_activities = db.list_activities_types()

    for n_chips in range(2,6):
        for activity in list_of_activities:
            if activity["Name"].find("New Ladder GLUE "+str(n_chips)) != -1:
                generic_name = "LADDER-GLUE-"+str(n_chips)
                file.write(str(generic_name)+"="+str(activity["ID"])+"\n")
                get_more_type_ids_related_to_activity(db, activity["ID"], generic_name)

            if activity["Name"].find("New Ladder CHK VISU "+str(n_chips)) != -1:
                generic_name = "LADDER-CHK-VISU-"+str(n_chips)
                file.write(str(generic_name)+"="+str(activity["ID"])+"\n")
                get_more_type_ids_related_to_activity(db, activity["ID"], generic_name)

            if activity["Name"].find("New Ladder BOND "+str(n_chips)) != -1:
                generic_name = "LADDER-BOND-"+str(n_chips)
                file.write(str(generic_name)+"="+str(activity["ID"])+"\n")
                get_more_type_ids_related_to_activity(db, activity["ID"], generic_name)
            if activity["Name"].find("New Ladder COND "+str(n_chips)) != -1:
                generic_name = "LADDER-COND-"+str(n_chips)
                file.write(str(generic_name)+"="+str(activity["ID"])+"\n")
                get_more_type_ids_related_to_activity(db, activity["ID"], generic_name)

            if activity["Name"].find("New Ladder CHK VISBON "+str(n_chips)) != -1:
                generic_name = "LADDER-CHK-VISBON-"+str(n_chips)
                file.write(str(generic_name)+"="+str(activity["ID"])+"\n")
                get_more_type_ids_related_to_activity(db, activity["ID"], generic_name)
            if activity["Name"].find("New Ladder CHK SMOKE "+str(n_chips)) != -1:
                generic_name = "LADDER-CHK-SMOKE-"+str(n_chips)
                file.write(str(generic_name)+"="+str(activity["ID"])+"\n")
                get_more_type_ids_related_to_activity(db, activity["ID"], generic_name)
            if activity["Name"].find("New Ladder CHK ELEC "+str(n_chips)) != -1:
                generic_name = "LADDER-CHK-ELEC-"+str(n_chips)
                file.write(str(generic_name)+"="+str(activity["ID"])+"\n")
                get_more_type_ids_related_to_activity(db, activity["ID"], generic_name)
            if activity["Name"].find("New Ladder CHK FUNC "+str(n_chips)) != -1:
                generic_name = "LADDER-CHK-FUNC-"+str(n_chips)
                file.write(str(generic_name)+"="+str(activity["ID"])+"\n")
                get_more_type_ids_related_to_activity(db, activity["ID"], generic_name)
    file.close()

    return 1

def get_components_type_ids(db):
    """Print to file type IDs of all components  in DB.
    Note, that order in the output file depends on the order of component in database (and not this local order).
    """

    file = open("localData/type_ids/componentsTypeIDs.txt", "w")
    file.write("[Object]\n")

    list_of_components = db.list_of_component_type()

    for component in list_of_components:
        if component["Name"].find("ALPIDEB Chip") != -1:
            file.write("ALPIDEB-Chip="+str(component["ID"])+"\n")
        if component["Name"].find("ALPIDEB Tray") != -1:
            file.write("ALPIDEB-Tray="+str(component["ID"])+"\n")
        for n_chips in range(2,6):
            if component["Name"].find("new_ladder_"+str(n_chips)) != -1:
                file.write("LADDER-"+str(n_chips)+"="+str(component["ID"])+"\n")
            if component["Name"].find("new_FPC_"+str(n_chips)) != -1:
                file.write("FPC-"+str(n_chips)+"="+str(component["ID"])+"\n")

    file.close()
    return 1



if __name__ == '__main__':
    start_time = time.time()
    rc = main()
    print("--- %s seconds ---" % (time.time() - start_time))
    sys.exit(rc)
