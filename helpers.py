"""This place serves to contain standalone functions
"""
from datetime import datetime
import sys

def today_date():
    """Return today date in format for database
    """
    return datetime.now().strftime("%Y-%m-%d")

def give_yes_no_question(question):
    """Return true or false on question.
    """
    correct_answer = False
    answer = ""
    while not correct_answer:
        answer = str(input(str("\n" + question + " (yes/no)")))
        if answer == "yes" or answer == "Yes" or answer == "YES" or answer == "y" or answer == "Y"\
                or answer == "NO" or answer == "No" or answer == "no" or answer == "N" or answer == "n":
            correct_answer = True
        else:
            print("****** ERROR: Wrong answer! You can use only YES, Yes, yes, y, Y, NO, No, no, N or n. ******")

    if answer == "yes" or answer == "Yes" or answer == "YES" or answer == "y" or answer == "Y":
        return True
    else:
        return False

def test_correct_ladder_number(ladder_number):
    """Function to test, if ladder number is reasonable.
    """
    if not 2000 < int(ladder_number) < 6000:
        print("****** ERROR: Ladder number with 2000 and 6000 expected! ******")
        return False

    return True

def get_n_chips_from_ladder_number(ladder_number):
    """Returns number of chips from ladder number.
    """
    if not len(str(ladder_number)) == 4:
        sys.exit("****** ERROR: Ladder number with 4 digits expected! ******")

    if not test_correct_ladder_number(ladder_number):
        sys.exit()

    return int(str(ladder_number)[0])

def print_hello():
    """Just prints hello.
    """
    print_hashtags()
    print("\n############ HELLO ################")
    print_hashtags()

    return 1

def print_hashtags():
    """Just prints 3 lines of hashtags.
    """
    print("\n###################################")
    print("###################################")
    print("###################################")

    return 1



