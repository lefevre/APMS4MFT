#Permet de sauvegarder les activités pour tout les Chips.

from zeep import Client
from zeep.transports import Transport
import http.cookiejar
import sys 
import subprocess 
import configparser
import os
import re # parse regular expression 
import base64
import logging
from APMSdatabase import APMSdatabase 
from setAPMSenv import SetApmsEnv
import lxml
import datetime
import ast

def setLogger():
    global myLogger
    logFormat="%(funcName)-12s %(asctime)-24s %(process)d %(levelname)-6s %(message)s"
    logging.basicConfig(format=logFormat,level=logging.DEBUG)
    myLogger=logging.getLogger()
    
def fctSortDict(value):
    return value["Date"]

def main():
#Set the environment (configuration files are in the folder: "config")
  setEnv=SetApmsEnv.SetApmsEnv()
  setLogger()
  envDict=setEnv.setup_environment_dict
  DATABASE=envDict.get("DATABASE")
  PROJECT=envDict.get("PROJECT")
  PROJECT_ID=int(envDict.get("PROJECT_ID"))
  USERID=int(envDict.get("USERID"))
  WSDL=envDict.get("WSDL")
  LOCATION=envDict.get("LOCATION")
  CHIP=envDict.get("ALPIDEB-CHIP")#367 pour ITSP
  TRAY=envDict.get("ALPIDEB-TRAY")#387 pour ITSP
  APMStest=APMSdatabase.APMSdatabase(PROJECT,PROJECT_ID,USERID,WSDL)
  
#Reading the file who contains all Chips
  count=0
  fichier = open("DataFile/ChipsInMFT.txt","r") #Chips.txt contains all Chips of one database. This file was create by GetAllChips.py
  liste=fichier.read()#here type(liste) = 'str'
  liste=ast.literal_eval(liste) #Change liste type to 'list'
  fichier.close()
  listeRes=[]

#API Querying
  for i in liste:
    for j in i:
      littleTab=[]
      bigTab=[]
      print(j["ID"])
      What=j["ID"]
      res=APMStest.readAllComponentActivities(What)
      print(res)
      bigTab.append(res)
      bigTab.append(j["ID"])
      listeRes.append(bigTab)
      count=count+1
      if count==200: #Add to minimize tests time
        break
        
        
  liste=listeRes #<--------------------------------------------------------------------------------------------------------
  listTrier=[]
  
  #In liste there are two objects of type 'list'.
  #The first one contains the ID of a component
  #And the second contain a list which contains dictionnaries with all activities of the same component.   
  for tab in liste:
    dico={}
    for i in range(2):
      print(i)
      
      #Processing of the first list
      if type(tab[i])==type(1): 
        IDComposant=tab[1]
        dico["IdComponent"]=IDComposant
        
      #Processing of the second
      if type(tab[i])==type([]):
        for activity in tab[0]:
          lId=activity["ActivityID"]
          leNom=activity["ActivityName"]
          laDate=activity["ActivityStartDate"]
          dateInDB=activity["ActivityStartDate"]
          param=activity["ActivityType"]["Parameters"]
          
          #Remove spaces:
          leNom=leNom.replace(" ","")
          
          #Replace all "." by "-"
          laDate=laDate.replace(".","-")
          laDate=datetime.datetime.strptime(laDate, '%d-%m-%Y')
          
          #Dico filling
          dico["Id"]=lId
          dico["Date"]=laDate
          dico["Nom"]=leNom
          dico["DateInDB"]=dateInDB
          dico["Parametre"]=param
      
    #listTrier filling
    listTrier.append(dico)
  #listTrier sorting
  listTrier=sorted(listTrier,key=fctSortDict)
  
  #Get last update date
  newFile=open("DataFile/LastUpdateActChips.txt","r")
  lastUpdateDate=newFile.read()
  newFile.close()
  
  #Replace inconveniences
  lastUpdateDate=str(lastUpdateDate)
  lastUpdateDate=lastUpdateDate.replace(".","-")
  lastUpdateDate=lastUpdateDate.replace(" ","")
  lastUpdateDate=lastUpdateDate.rstrip()#Essential
  lastUpdateDate=datetime.datetime.strptime(lastUpdateDate,'%d-%m-%Y')#traduction in a date format
  listTrierAndFiltrer=[]
  for oneAct in listTrier:
    print(lastUpdateDate-oneAct["Date"])
    #listTrierAndFiltrer filling
    if (lastUpdateDate<oneAct["Date"])==True:
      listTrierAndFiltrer.append(oneAct)
      print("ALELOUIA")
      
  #listTrierAndFiltrer sorting
  listTrierAndFiltrer=sorted(listTrierAndFiltrer,key=fctSortDict)
  print(listTrierAndFiltrer)
  
  #Save result in a file
  fichier2=open("DataToAdd/ActivitiesChipsToAdd.txt","w")
  for oneAct in listTrierAndFiltrer:
    fichier2.writelines(str(oneAct))
  fichier2.close()  
  sys.exit()
  
  
  
  
  
if __name__=='__main__':
  rc=main()
  sys.exit(rc)
