"""This script serves purely for testing
"""

import sys
import time

# local scripts
import helpers
import get_type_ids
from manager_ladder import LadderManager
from manager_fpc import FPCManager
from manager_chip import ChipManager
from manager_documents import DocumentsManager
from manager_activity import ActivityManager
from setAPMSenv import SetApmsEnv

# global setting
use_local_storage = False
is_testing_setup = True
environment_setup = SetApmsEnv.SetApmsEnv()
environment_dict = environment_setup.setup_environment_dict

def ground_logic(is_testing_setup_=False):
    """Ground logic. This contains everything.
    """
    activity_manager = ActivityManager(environment_setup,use_local_storage)

    # Set options
    options = ["Nothing. Close this.",
               "Perform an activity.",
               "Change an activity.",
               "Create in database components from traveler.",
               "Get type IDs from database."]
    answers = -1

    menu_on = True
    while menu_on:
        incorrect_choice = True
        while incorrect_choice:
            helpers.print_hashtags()
            print("\nWhat do you want to do (Choose a number)?")
            for i_option in range(len(options)):
                print("\n Option " + str(i_option) + " : " + str(options[i_option]))
            choice = input("\nEnter a number: ")

            passed_integer_control = False
            try:
                int(choice)
                passed_integer_control = True
            except ValueError:
                print("\n****** ERROR: Enter an integer! ******")

            if passed_integer_control:
                for i_choice in range(len(options)):
                    if int(choice) == i_choice:
                        answers = i_choice
                        incorrect_choice = False

                if incorrect_choice:
                    print("\n****** ERROR: Incorrect choice. Please, choose one in range 0 to "
                          + str(len(options) - 1) + "! ******")

        # Block for actions
        if answers == 0:
            menu_on = False
            print("\nSee you next time...")
        if answers == 1:
            activity_manager.perform_activity_on_component(is_testing_setup_)
        if answers == 2:
            activity_manager.change_activity(is_testing_setup_)
        if answers == 3:
            create_component_from_hic_traveler(is_testing_setup_)
        if answers == 4:
            print(get_type_ids.main())

    return "\nAll tasks done!"

def create_component_from_hic_traveler(is_testing_setup_):
    """Function to create ladder and its FPC and chips if necessary.
    It also attaches  components to the ladder and set chip position of chips on ladder."""
    print("\nCreating new ladder and attaching components to it.")

    # Setup
    ladder_number = int(input("\nEnter the ladder number (i.e. 3154). Pay attention to input correctly! "))

    n_chips = helpers.get_n_chips_from_ladder_number(ladder_number)

    if is_testing_setup_:
        ladder_component_name = "HIC_" + str(ladder_number) + "-test"
    else:
        ladder_component_name = "HIC_" + str(ladder_number)


    manager_ladder = LadderManager(environment_dict,n_chips,use_local_storage)
    manager_fpc = FPCManager(environment_dict,n_chips,use_local_storage)
    manager_chip = ChipManager(environment_dict,use_local_storage)
    manager_traveler = DocumentsManager(ladder_number)

    # Create the new ladder
    ladder_db_id = manager_ladder.create_new_ladder(ladder_component_name)

    # Assign and create (if necessary) new FPCs
    fpc_db_id = manager_fpc.get_fpc_db_id_from_hic_traveler(manager_traveler.hic_traveler)
    if fpc_db_id == -999:
        print("FPC not found in database. Creating a new one. Source: HIC traveler.")
        fpc_db_id = manager_fpc.create_new_fpc(
            manager_fpc.get_fpc_component_name_from_hic_traveler(manager_traveler.hic_traveler))
    manager_ladder.db.place_component_in_composition(ladder_db_id, fpc_db_id, "", -1)

    # Assign and create (if necessary) new chips
    list_of_chip_component_ids = manager_chip.get_list_of_chip_component_ids(manager_traveler.hic_traveler)
    for chip in list_of_chip_component_ids:
        chip_db_id = manager_chip.get_chip_db_id_from_component_id(chip + "-MFT")
        if chip_db_id == -999:
            print("Chip not found in database. Creating a new one. Source: HIC traveler.")
            chip_db_id = manager_chip.create_new_chip(chip + "-MFT")
        manager_ladder.db.place_component_in_composition(ladder_db_id, chip_db_id, "", -1)

    # Assigning positions
    manager_chip.set_all_chip_positions_on_ladder(ladder_db_id, manager_traveler.hic_traveler)

    return



if __name__ == '__main__':
    start_time = time.time()
    # rc = main()
    helpers.print_hello()
    rc = ground_logic(is_testing_setup)
    print("\n--- %s seconds ---" % (time.time() - start_time))
    sys.exit(rc)
