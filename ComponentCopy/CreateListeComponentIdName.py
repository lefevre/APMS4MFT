# list all components in DB and save results in output

# sorted tested with fake value -> ok

from zeep import Client
from zeep.transports import Transport
import http.cookiejar
import datetime
import sys
import subprocess
import configparser
import os
import re  # parse regular expression
import base64
import logging
from APMSdatabase import APMSdatabase
from setAPMSenv import SetApmsEnv
import lxml
import ast


def setLogger():
    global myLogger
    logFormat = "%(funcName)-12s %(asctime)-24s %(process)d %(levelname)-6s %(message)s"
    logging.basicConfig(format=logFormat, level=logging.DEBUG)
    myLogger = logging.getLogger()


def fctSortDict(one_component):
    return one_component['Id']


def main():
    setEnv = SetApmsEnv.SetApmsEnv()
    setLogger()

    envDict = setEnv.setup_environment_dict

    DATABASE = envDict.get("DATABASE")
    PROJECT = envDict.get("PROJECT")
    PROJECT_ID = int(envDict.get("PROJECT_ID"))
    USERID = int(envDict.get("USERID"))
    WSDL = envDict.get("WSDL")
    LOCATION = envDict.get("LOCATION")

    APMStest = APMSdatabase.APMSdatabase(PROJECT, PROJECT_ID, USERID, WSDL)

    component_type = 987  # test database tray
    res = APMStest.list_component_of_type(component_type)
    # print(res)

    sorted_named_list = []

    for component in res:
        dico = {}
        one_id = component["ID"]
        one_name = component["ComponentID"]
        one_name = one_name.replace(" ", "")
        dico["Id"] = one_id
        dico["Nom"] = one_name
        sorted_named_list.append(dico)

    sorted_named_list = sorted(sorted_named_list, key=fctSortDict)  # appel la fonction de trie

    output_file = open("output/tray_test_name_id.txt", "w")
    output_file.write(str(sorted_named_list))
    output_file.close()

    sys.exit()


if __name__ == '__main__':
    rc = main()
    sys.exit(rc)
