#!/usr/bin/python

# imports I understand why they are here
from zeep import Client
from zeep.transports import Transport
import http.cookiejar
import datetime
from datetime import datetime
import sys 
import subprocess 
import configparser
import os
import re # parse regular expression 
import base64
import logging
import lxml
from setAPMSenv import SetApmsEnv
from APMSdatabase import APMSdatabase 



def setLogger():
    global myLogger
    logFormat="%(funcName)-12s %(asctime)-24s %(process)d %(levelname)-6s %(message)s"
    logging.basicConfig(format=logFormat,level=logging.DEBUG)
    myLogger=logging.getLogger()



def main():


  setEnv=SetApmsEnv.SetApmsEnv()
  setLogger()
  envDict=setEnv.setup_environment_dict
  # both below are not yet ready
#  componentDict=setEnv.setupComponentDict()
#  activityDict=setEnv.setupActivityDict()
  print("setup found:", envDict)


  PROJECT=envDict.get("PROJECT")
  PROJECT_ID=int(envDict.get("PROJECT_ID"))
  USERID=int(envDict.get("USERID"))
  WSDL=envDict.get("WSDL")
  LOCATION=envDict.get("LOCATION")




  APMStest=APMSdatabase.APMSdatabase(PROJECT,PROJECT_ID,USERID,WSDL)


#  myLogger.info(LOCATION) 

  myLogger.info(APMStest.baseID)
  myLogger.info(APMStest)



#warning code sample only at this point 


# how to list all activities of one particular type 

  alpideChipPhysicalCheckup=1723
  actList=APMStest.list_activities_of_type(alpideChipPhysicalCheckup)
  print("activity list for Alpide Chip Physical Checkup")
  print(actList) 
  
#  sys.exit()


# how to list component of one particular type and read all element of that type
  # list tray it is faster, compId to be set in .cfg file 
  trayID=987
  componentList=APMStest.list_component_of_type(trayID)

#  print(componentList)
  print('')
  print('')


  for oneComponent in componentList:


    componentID=oneComponent["ID"]
    emptyString=""
    print("component ->",componentID,emptyString)
    componentDetails=APMStest.show_component(componentID, emptyString)
    print("component content ->",componentDetails)
    sys.exit()

#  sys.exit()

if __name__=='__main__':
  rc=main()
  sys.exit(rc)
