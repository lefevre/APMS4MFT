#!/usr/bin/python

#Récupère la liste des activité qu'a subit un composant  

#Test de killian

from zeep import Client
from zeep.transports import Transport
import http.cookiejar
import sys 
import subprocess 
import configparser
import os
import re # parse regular expression 
import base64
import logging
from APMSdatabase import APMSdatabase 
from setAPMSenv import SetApmsEnv
import lxml
import datetime

def setLogger():
    global myLogger
    logFormat="%(funcName)-12s %(asctime)-24s %(process)d %(levelname)-6s %(message)s"
    logging.basicConfig(format=logFormat,level=logging.DEBUG)
    myLogger=logging.getLogger()
    

def fctSortDict(value):
    return value["Date"]

def main():
  setEnv=SetApmsEnv.SetApmsEnv()
  setLogger()

  envDict=setEnv.setup_environment_dict

  DATABASE=envDict.get("DATABASE")
  PROJECT=envDict.get("PROJECT")
  PROJECT_ID=int(envDict.get("PROJECT_ID"))
  USERID=int(envDict.get("USERID"))
  WSDL=envDict.get("WSDL")
  LOCATION=envDict.get("LOCATION")



  APMStest=APMSdatabase.APMSdatabase(PROJECT,PROJECT_ID,USERID,WSDL)

  #Requete l'API
  print ("select Component:")
  What=sys.stdin.readline()
  print ("list of all activities of the component :")
  res=APMStest.readAllComponentActivities(What)
  print(res)
  
  #Permet de filtrer la liste obtenu dans res.
  listTrier=[]
  for activity in res:
    #La liste est constituée de plusieurs dictionnaire
    dico={}
    lId=activity["ActivityID"]
    leNom=activity["ActivityName"]
    laDate=activity["ActivityStartDate"]
    dateInDB=activity["ActivityStartDate"]
    #Permet de supprimer ls espaces dans leNom:
    leNom=leNom.replace(" ","")
    
    #Essai remplacement de la date
    laDate=laDate.replace(".","-")
    laDate=datetime.datetime.strptime(laDate, '%d-%m-%Y')
    
    #Chaque dictionnaire dispose de clé "Id" et "Nom"
    dico["Id"]=lId
    dico["Date"]=laDate
    dico["Nom"]=leNom
    dico["DateInDB"]=dateInDB
    listTrier.append(dico)
    listTrier=sorted(listTrier,key=fctSortDict,reverse=True) #appel la fonction de trie
  print(listTrier)  
  
  sys.exit()
  
  
if __name__=='__main__':
  rc=main()
  sys.exit(rc)
