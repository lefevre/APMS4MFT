#This script create a CHIP in the test Database
  
  
from zeep import Client
from zeep.transports import Transport
import http.cookiejar
import sys 
import subprocess 
import configparser
import os
import re # parse regular expression 
import base64
import logging
from APMSdatabase import APMSdatabase 
from setAPMSenv import SetApmsEnv
import lxml
import datetime
import ast


def setLogger():
    global myLogger
    logFormat="%(funcName)-12s %(asctime)-24s %(process)d %(levelname)-6s %(message)s"
    logging.basicConfig(format=logFormat,level=logging.DEBUG)
    myLogger=logging.getLogger()


def main():
#Set the environment (configuration files are in the folder: "config")
  setEnv=SetApmsEnv.SetApmsEnv()
  envDict=setEnv.setup_environment_dict
  DATABASE=envDict.get("DATABASE")
  PROJECT=envDict.get("PROJECT")
  PROJECT_ID=int(envDict.get("PROJECT_ID"))
  USERID=int(envDict.get("USERID"))
  WSDL=envDict.get("WSDL")
  LOCATION=envDict.get("LOCATION")
  CHIP=envDict.get("ALPIDEB-CHIP")#367 pour ITSP
  TRAY=envDict.get("ALPIDEB-TRAY")#387 pour ITSP
  APMStest=APMSdatabase.APMSdatabase(PROJECT,PROJECT_ID,USERID,WSDL)


  fichier = open("DataToAdd/ChipsToAdd.txt","r") #Chips.txt contains all Chips of one database. This file was create by GetAllChips.py
  liste=fichier.read()#here type(liste) = 'str'
  liste=ast.literal_eval(liste) #Change liste type to 'list'
  fichier.close()
  count=0
  for unDict in liste:
    unComponentID=unDict["ComponentID"]
    unSupplierComponentID=unDict["SupplierComponentID"]
    uneDescription=unDict["Description"]
    unLotID=unDict["LotID"]
    unPackageID=unDict["PackageID"]
    count+=1
    print(type(unComponentID))
    if uneDescription==None:
      uneDescription=""
                      #(ComponentType,ComponentID,SupplierComponentID,Description,LotID,PackageID,userID)
    res=APMStest.new_component(967, unComponentID + str("-MFT"), unSupplierComponentID, uneDescription, unLotID, unPackageID, -1)
    #res=APMStest.newComponent(967,"killianTest6","its",uneDescription,"0","0",-1)
    print(DATABASE)
    print(res)
    
if __name__=='__main__':
  rc=main()
  sys.exit(rc)

