"""This module is intended to contain all methods related to activity manipulation.
"""

__author__ = "Roman Lavicka"

# standard classes
import ast
import sys
from datetime import datetime

# local scripts
import helpers
from manager_ladder import LadderManager
from manager_fpc import FPCManager
from manager_chip import ChipManager
from setAPMSenv import SetApmsEnv
from manager_documents import DocumentsManager
from APMSdatabase import APMSdatabase

# global variables for this module
type_id_act = -1
type_id_loc_cern = -1
type_id_loc_ipnl = -1
type_id_stat_open = -1
type_id_stat_closed = -1
type_id_res_ok = -1
type_id_res_nok = -1

type_id_output_ladder = -1
type_id_output_fpc = -1
type_id_input_ladder = -1
type_id_input_fpc = -1
type_id_input_chip = -1

type_id_param_gold = -1
type_id_param_silver = -1
type_id_param_bronze = -1
type_id_param_bad = -1

use_local_storage = False
n_chips = 3
environment_setup = SetApmsEnv.SetApmsEnv()
environment_dict = environment_setup.setup_environment_dict
db = APMSdatabase.APMSdatabase(
        environment_dict.get("PROJECT"),
        int(environment_dict.get("PROJECT_ID")),
        int(environment_dict.get("USERID")),
        environment_dict.get("WSDL"),
        )

class ActivityManager:
    """Manager to control activities in database.
    """

    def __init__(self, environment_setup_, use_local_storage_):
        self.environment_setup = environment_setup_
        self.environment_dict = environment_setup.setup_environment_dict
        self.db = APMSdatabase.APMSdatabase(
                environment_dict.get("PROJECT"),
                int(environment_dict.get("PROJECT_ID")),
                int(environment_dict.get("USERID")),
                environment_dict.get("WSDL"),
                )
        print(self.switch_data_storage(use_local_storage_))
        self.n_chips = 3  # Use setter to change this - be careful, it is a decision tree

        self.type_id_act = -1
        self.type_id_loc_cern = -1
        self.type_id_loc_ipnl = -1
        self.type_id_stat_open = -1
        self.type_id_stat_closed = -1
        self.type_id_res_ok = -1
        self.type_id_res_nok = -1

        self.type_id_output_ladder = -1
        self.type_id_output_fpc = -1
        self.type_id_input_ladder = -1
        self.type_id_input_fpc = -1
        self.type_id_input_chip = -1

        self.type_id_param_gold = -1
        self.type_id_param_silver = -1
        self.type_id_param_bronze = -1
        self.type_id_param_bad = -1

        return

    def set_n_chips(self):
        """Set number of chips of this activity.
        Important to handle correct activity, fpc and ladder definitions.
        """
        incorrect_choice = True
        n_chips_ = ""
        while incorrect_choice:
            n_chips_ = str(input("\nFor how many chips is your component designed? "))

            passed_integer_control = False
            try:
                int(n_chips)
                passed_integer_control = True
            except ValueError:
                print("\n****** ERROR: Enter an integer! ******")

            if passed_integer_control:
                if 1 < int(n_chips) < 6:
                    incorrect_choice = False
                else:
                    print("\n****** ERROR: Incorrect choice. Please, choose in range from 2 to 5! ******")
        self.n_chips = n_chips_

        return self.n_chips

    @staticmethod
    def switch_data_storage(on_off):
        """Function to switch between local storage and other.
        If you switch it on, local databasis are NOT actualized.
        """
        message = "Activity Manager set to use remote database."
        global use_local_storage

        use_local_storage = on_off

        if use_local_storage:
            message = "Activity Manager set to use local database."

        return message

    def set_assembly_component_type_ids(self, activity_type_name):
        """Function to set up component type_ids for assembly activity.
        """
        if not activity_type_name:
            print("Activity name empty. Cannot set type IDs.")
            return False

        set_env = SetApmsEnv.SetApmsEnv()
        env_dict = set_env.setup_environment_dict

        self.type_id_output_ladder = env_dict.get("COM-" + str(activity_type_name) + "-OUT")
        self.type_id_input_fpc = env_dict.get("COM-" + str(activity_type_name) + "-IN-FPC")
        self.type_id_input_chip = env_dict.get("COM-" + str(activity_type_name) + "-IN-CHIP")

        return True

    def set_type_ids(self, activity_type_name):
        """Function to set up type_ids based on activity type.
        """
        if not activity_type_name:
            print("Activity name empty. Cannot set type IDs.")
            return False

        set_env = SetApmsEnv.SetApmsEnv()
        env_dict = set_env.setup_environment_dict
        self.type_id_act = env_dict.get(str(activity_type_name))
        self.type_id_loc_cern = env_dict.get("LOC-" + str(activity_type_name) + "-CERN")
        self.type_id_loc_ipnl = env_dict.get("LOC-" + str(activity_type_name) + "-IPNL")
        self.type_id_stat_open = env_dict.get("STATUS-OPEN")
        self.type_id_stat_closed = env_dict.get("STATUS-CLOSED")
        self.type_id_res_ok = env_dict.get("RES-" + str(activity_type_name) + "-OK")
        self.type_id_res_nok = env_dict.get("RES-" + str(activity_type_name) + "-NOK")

        self.type_id_output_ladder = env_dict.get("COM-" + str(activity_type_name) + "-OUT")
        self.type_id_output_fpc = env_dict.get("COM-" + str(activity_type_name) + "-OUT")
        self.type_id_input_ladder = env_dict.get("COM-" + str(activity_type_name) + "-IN")
        self.type_id_input_fpc = env_dict.get("COM-" + str(activity_type_name) + "-IN")

        self.type_id_param_gold = env_dict.get("PAR-" + str(activity_type_name) + "-GOL")
        self.type_id_param_silver = env_dict.get("PAR-" + str(activity_type_name) + "-SIL")
        self.type_id_param_bronze = env_dict.get("PAR-" + str(activity_type_name) + "-BRO")
        self.type_id_param_bad = env_dict.get("PAR-" + str(activity_type_name) + "-BAD")

        if self.type_id_act is not None:
            return True
        else:
            print("\n****** ERROR: Something is wrong with setting of type IDs. ******")
            return False

    def change_activity_status_to_closed(self, activity_type_name, activity_name):
        """Function. Should lead user to change activity status to closed.
        """

        # Prepare input data
        set_ids = self.set_type_ids(activity_type_name)
        if not set_ids:
            sys.exit("IDs were not defined!")

        activity = self.db.find_activity_with_name(self.type_id_act, activity_name)

        if helpers.give_yes_no_question("Do you really want to close this activity?"
                                        " You will not be able to open it again!"):
            db.activity_change(self.get_activity_id(activity),self.get_activity_type_id(activity),
                               self.get_activity_location_type_id(activity), "", activity_name,
                               self.get_activity_startdate(activity), helpers.today_date(), "",
                               self.get_activity_result_type_id(activity), self.type_id_stat_closed, -1)
        else:
            print("Activity remains open.")

        return 1

    def change_activity_result(self, activity_type_name, activity_name):
        """Function. Should lead user to change activity result.
        """

        # Prepare input data
        set_ids = self.set_type_ids(activity_type_name)
        if not set_ids:
            sys.exit("IDs were not defined!")

        activity = self.db.find_activity_with_name(self.type_id_act, activity_name)

        if helpers.give_yes_no_question("Is the result of this activity ok?"):
            db.activity_change(self.get_activity_id(activity),self.get_activity_type_id(activity),
                               self.get_activity_location_type_id(activity), "", activity_name,
                               self.get_activity_startdate(activity), helpers.today_date(), "",
                               self.type_id_res_ok, self.type_id_stat_open, -1)
        elif helpers.give_yes_no_question("Is the result of this activity not ok?"):
            db.activity_change(self.get_activity_id(activity),self.get_activity_type_id(activity),
                               self.get_activity_location_type_id(activity), "", activity_name,
                               self.get_activity_startdate(activity), helpers.today_date(), "",
                               self.type_id_res_nok, self.type_id_stat_open, -1)
        else:
            print("Why are you calling this function? No change will be performed.")

        return 1

    def change_activity_location(self, activity_type_name, activity_name):
        """Function. Should lead user to change activity location.
        """
        # Prepare input data
        set_ids = self.set_type_ids(activity_type_name)
        if not set_ids:
            sys.exit("IDs were not defined!")

        activity = self.db.find_activity_with_name(self.type_id_act, activity_name)

        location_type_id = self.options_location(activity_type_name)

        db.activity_change(self.get_activity_id(activity),self.get_activity_type_id(activity),
                           location_type_id, "", activity_name,
                           self.get_activity_startdate(activity), helpers.today_date(), "",
                           self.get_activity_result_type_id(activity), self.type_id_stat_open, -1)

        return 1

    def change_activity_classification(self, activity_type_name, activity_name):
        """Funtion to lead user to change activity clasification.
        """
        # Prepare input data
        set_ids = self.set_type_ids(activity_type_name)
        if not set_ids:
            sys.exit("IDs were not defined!")

        activity_db_id = self.get_activity_id(self.db.find_activity_with_name(self.type_id_act, activity_name))

        type_id_param = self.options_classification_parameter(activity_type_name)

        # Add category of result (bad, bronze, silver, gold)
        self.db.activity_parameter_create(activity_db_id,type_id_param,-1,-1)

        return 1

    def change_activity(self, is_testing_setup=False):
        """General function. Should lead user to find an existing activity and changing its attribute.
        """

        # Start selection
        activity_type_name = self.activity_decision_tree()

        # Prepare input data
        set_ids = self.set_type_ids(activity_type_name)
        if not set_ids:
            sys.exit("IDs were not defined!")

        if activity_type_name.find("FPC") is not -1:
            fpc_number = str(input("\nEnter the FPC number (i.e. 24aA102). Pay attention to input correctly! "))
            if is_testing_setup:
                fpc_component_name = "FPC_" + str(fpc_number) + "-test"
            else:
                fpc_component_name = "FPC_" + str(fpc_number)
            activity_name = self.get_activity_name(activity_type_name, fpc_component_name)
        else:
            hic_number = int(input("\nEnter the ladder number (i.e. 3154). Pay attention to input correctly! "))
            if is_testing_setup:
                ladder_component_name = "HIC_" + str(hic_number) + "-test"
            else:
                ladder_component_name = "HIC_" + str(hic_number)
            activity_name = self.get_activity_name(activity_type_name, ladder_component_name)

        if not self.db.activity_name_exists(self.type_id_act, activity_name):
            sys.exit("Activity does not exists in the database!")

        # BLOCK TO CHOOSE WHAT WILL CHANGE
        incorrect_choice = True
        chosen_change = 0
        while incorrect_choice:
            print("\nWhat do you want to change?")
            print("\n Option 0 : I changed my mind. Do nothing.")
            print("\n Option 1 : Change status to closed.")
            print("\n Option 2 : Change activity result.")
            print("\n Option 3 : Change activity location.")
            print("\n Option 4 : Change activity classification.")
            n_options = 5
            chosen_change = str(input("\nYour input: "))

            passed_integer_control = False
            try:
                int(chosen_change)
                passed_integer_control = True
            except ValueError:
                print("\n****** ERROR: Enter an integer! ******")

            if passed_integer_control:
                if int(chosen_change) < n_options:
                    incorrect_choice = False
                else:
                    print("\n****** ERROR: Incorrect choice. Please, choose one in range 0 to "
                          + str(n_options - 1) + "! ******")

        # BLOCK TO PERFORM CHOSEN CHANGE
        if int(chosen_change) == 0:
            print("\nTerminating this function...")
            return 0

        if int(chosen_change) == 1:
            self.change_activity_status_to_closed(activity_type_name,activity_name)
            return 1

        if int(chosen_change) == 2:
            self.change_activity_result(activity_type_name,activity_name)
            return 2

        if int(chosen_change) == 3:
            self.change_activity_location(activity_type_name,activity_name)
            return 3

        if int(chosen_change) == 4:
            self.change_activity_classification(activity_type_name,activity_name)
            return 4


    def get_activity_name(self, activity_type_name, component_name):
        """Return activity description related to activity type.
        Break the ongoing script, if wrong naming.
        """
        if not self.is_activity_type_name_allowed(activity_type_name):
            sys.exit("Wrong definition of activity name!")

        if activity_type_name.find("FPC-SOLD") != -1:
            return "Soldering of " + str(component_name)
        if activity_type_name.find("FPC-CHK-RAW-VISU") != -1:
            return "Visual check of " + str(component_name)
        if activity_type_name.find("FPC-CHK-RAW-ELEC") != -1:
            return "Electrical check of " + str(component_name)
        if activity_type_name.find("FPC-CHK-RAW-METR") != -1:
            return "Metrological check of " + str(component_name)
        if activity_type_name.find("FPC-CHK-EQU-VISU") != -1:
            return "Visual check of  " + str(component_name)
        if activity_type_name.find("FPC-CHK-EQU-CLEA") != -1:
            return "Cleaning of" + str(component_name)
        if activity_type_name.find("FPC-CHK-EQU-METR") != -1:
            return "Metrological check of " + str(component_name)
        if activity_type_name.find("FPC-CHK-EQU-ELEC") != -1:
            return "Electrical check of " + str(component_name)

        if activity_type_name.find("LADDER-GLUE") != -1:
            return "Gluing chips and FPC on " + str(component_name)
        if activity_type_name.find("LADDER-COND") != -1:
            return "Conditioning ladder " + str(component_name)
        if activity_type_name.find("LADDER-BOND") != -1:
            return "Bonding components on " + str(component_name)
        if activity_type_name.find("LADDER-CHK-VISU") != -1:
            return "Visual check after gluing of " + str(component_name)
        if activity_type_name.find("LADDER-CHK-VISBON") != -1:
            return "Visual check after bonding of " + str(component_name)
        if activity_type_name.find("LADDER-CHK-SMOKE") != -1:
            return "Smoke check of " + str(component_name)
        if activity_type_name.find("LADDER-CHK-ELEC") != -1:
            return "Electrical check of " + str(component_name)
        if activity_type_name.find("LADDER-CHK-FUNC") != -1:
            return "Functional check of " + str(component_name)

    @staticmethod
    def get_activity_id(activity):
        """Returns activity id in database from activity object. Used for better readability.
        """
        return activity['ID']

    @staticmethod
    def get_activity_type_id(activity):
        """Returns activity type id in database from activity object.
        """
        return activity['ActivityType']['ID']

    @staticmethod
    def get_activity_startdate(activity):
        """Returns startdate from activity object in format usable in database.
        """
        return datetime.strptime(activity['StartDate'], "%d.%m.%Y").strftime("%Y-%m-%d")

    def get_activity_result_type_id(self, activity):
        """Returns current result from activity object in format usable in database.
        """
        return self.db.read_one_activity(activity['ID'])['ActivityResult']['ID']

    def get_activity_location_type_id(self, activity):
        """Returns current location from activity object in format usable in database.
        """
        return self.db.read_one_activity(activity['ID'])['ActivityLocation']['ID']

    @staticmethod
    def is_activity_type_name_allowed(activity_type_name):
        """Check, if activity name was defined for this machinery.
        """
        input_file = open("config/allowed_activities.txt")
        allowed_activities = input_file.read()
        input_file.close()
        allowed_activities = ast.literal_eval(allowed_activities)

        for allowed_activity in allowed_activities:
            for n_chips_ in range(2,6):
                if activity_type_name.find(allowed_activity['activity'] + "-" + str(n_chips_)) != -1:
                    return True

        return False

    def perform_activity_on_component(self, is_testing_setup=False):
        """General function. Should lead the user through the process of inserting activity and its component to system.
        """

        activity_type_name = self.activity_decision_tree()

        if activity_type_name.find("FPC") is not -1:
            fpc_number = str(input("\nEnter the FPC number (i.e. 24aA102). Pay attention to input correctly! "))
            if is_testing_setup:
                fpc_component_name = "FPC_" + str(fpc_number) + "-test"
            else:
                fpc_component_name = "FPC_" + str(fpc_number)
            self.standard_fpc_activity(fpc_component_name, fpc_number, activity_type_name)
        else:
            hic_number = int(input("\nEnter the ladder number (i.e. 3154). Pay attention to input correctly! "))
            if is_testing_setup:
                ladder_component_name = "HIC_" + str(hic_number) + "-test"
            else:
                ladder_component_name = "HIC_" + str(hic_number)
            if activity_type_name.find("GLUE") is not -1:
                self.standard_ladder_activity(ladder_component_name, hic_number, activity_type_name, True)
            else:
                self.standard_ladder_activity(ladder_component_name, hic_number, activity_type_name)

        return 1

    def standard_fpc_activity(self, fpc_component_name, fpc_number, activity_type_name):
        """General function for standard fpc activity.
        """
        print("Creating activity type " + str(activity_type_name))
        # Prepare input data
        set_ids = self.set_type_ids(activity_type_name)
        if not set_ids:
            sys.exit("IDs were not defined!")

        manager_fpc = FPCManager(environment_dict,self.n_chips,use_local_storage)
        manager_fpc_form = DocumentsManager(fpc_number, False)

        date = manager_fpc_form.get_activity_start_date()
        uri = manager_fpc_form.get_uri()

        activity_name = self.get_activity_name(activity_type_name, fpc_component_name)

        # Decide over location
        type_id_loc = self.options_location(activity_type_name)

        # Decide over result
        if helpers.give_yes_no_question("Is the result of activity OK?"):
            type_id_res = self.type_id_res_ok
        else:
            type_id_res = self.type_id_res_nok

        # Decide over status
        if helpers.give_yes_no_question("Should this activity remain open?"):
            type_id_stat = self.type_id_stat_open
        else:
            type_id_stat = self.type_id_stat_closed

        # Decide over activity classification
        type_id_param = self.options_classification_parameter(activity_type_name)

        # Create activity
        activity = db.create_new_activity(self.type_id_act, type_id_loc, "",
                                          activity_name, date, "", "",
                                          type_id_res, type_id_stat, -1)
        activity_db_id = activity['ID']

        # Process components
        # Assign output fpc
        fpc_db_id = manager_fpc.get_fpc_db_id_from_fpc_form(manager_fpc_form.fpc_form)
        if fpc_db_id == -999:
            print("FPC not found in database. Creating a new one. Source: FPC form.")
            fpc_db_id = manager_fpc.create_new_fpc(
                manager_fpc.get_fpc_component_name_from_fpc_form(manager_fpc_form.fpc_form))
        self.db.activity_component_assign(fpc_db_id, activity_db_id, self.type_id_output_fpc, -1)

        # Assign input fpc
        # No need to check ladder_db_id, already known from before
        self.db.activity_component_assign(fpc_db_id, activity_db_id, self.type_id_input_fpc, -1)

        # Add url related to this
        self.db.activity_uri_create(activity_db_id, uri, "", -1)

        # Add category of result (bad, bronze, silver, gold)
        self.db.activity_parameter_create(activity_db_id,type_id_param,-1,-1)

        return 1


    def standard_ladder_activity(self, ladder_component_name, ladder_number, activity_type_name, is_assembly=False):
        """General function for standard ladder activity.
        """
        print("Creating activity type " + str(activity_type_name))
        # Prepare input data
        set_ids = self.set_type_ids(activity_type_name)
        if not set_ids:
            sys.exit("IDs were not defined!")

        manager_ladder = LadderManager(environment_dict,self.n_chips,use_local_storage)
        manager_traveler = DocumentsManager(ladder_number)

        date = manager_traveler.get_activity_start_date()
        uri = manager_traveler.get_uri()

        activity_name = self.get_activity_name(activity_type_name,ladder_component_name)

        # Decide over location
        type_id_loc = self.options_location(activity_type_name)

        # Decide over result
        if helpers.give_yes_no_question("Is the result of activity OK? (yes/no)"):
            type_id_res = self.type_id_res_ok
        else:
            type_id_res = self.type_id_res_nok

        # Decide over status
        if helpers.give_yes_no_question("Should this activity remain open? (yes/no)"):
            type_id_stat = self.type_id_stat_open
        else:
            type_id_stat = self.type_id_stat_closed

        # Decide over activity classification
        type_id_param = self.options_classification_parameter(activity_type_name)

        # Create activity
        activity = db.create_new_activity(self.type_id_act, type_id_loc, "",
                                          activity_name, date, "", "",
                                          type_id_res, type_id_stat, -1)
        activity_db_id = activity['ID']

        # Process components
        if is_assembly:
            # Set special component type ids for assembly activity
            set_ids = self.set_assembly_component_type_ids(activity_type_name)
            if not set_ids:
                sys.exit("IDs were not defined!")
            manager_fpc = FPCManager(environment_dict,self.n_chips,use_local_storage)
            manager_chip = ChipManager(environment_dict,use_local_storage)

            # Assign output ladder
            ladder_db_id = manager_ladder.get_ladder_db_id_from_component_id(ladder_component_name)
            if ladder_db_id == -999:
                print("Ladder not found in database. Creating a new one. Source: HIC traveler.")
                ladder_db_id = manager_ladder.create_new_ladder(ladder_component_name)
            self.db.activity_component_assign(ladder_db_id, activity_db_id, self.type_id_output_ladder, -1)

            # Assign input FPC
            fpc_db_id = manager_fpc.get_fpc_db_id_from_hic_traveler(manager_traveler.hic_traveler)
            if fpc_db_id == -999:
                print("FPC not found in database. Creating a new one. Source: HIC traveler.")
                fpc_db_id = manager_fpc.create_new_fpc(
                    manager_fpc.get_fpc_component_name_from_hic_traveler(manager_traveler.hic_traveler))
            self.db.activity_component_assign(fpc_db_id, activity_db_id, self.type_id_input_fpc, -1)

            # Assign input chips
            list_of_chip_component_ids = manager_chip.get_list_of_chip_component_ids(manager_traveler.hic_traveler)
            for chip in list_of_chip_component_ids:
                chip_db_id = manager_chip.get_chip_db_id_from_component_id(chip + "-MFT")
                if chip_db_id == -999:
                    print("Chip not found in database. Creating a new one. Source: HIC traveler.")
                    chip_db_id = manager_chip.create_new_chip(chip + "-MFT")
                self.db.activity_component_assign(chip_db_id, activity_db_id, self.type_id_input_chip, -1)

        else:
            # Assign output ladder
            ladder_db_id = manager_ladder.get_ladder_db_id_from_component_id(ladder_component_name)
            if ladder_db_id == -999:
                print("Ladder not found in database. Creating a new one. Source: HIC traveler.")
                ladder_db_id = manager_ladder.create_new_ladder(ladder_component_name)
            self.db.activity_component_assign(ladder_db_id, activity_db_id, self.type_id_output_ladder, -1)

            # Assign input ladder
            # No need to check ladder_db_id, already known from before
            self.db.activity_component_assign(ladder_db_id, activity_db_id, self.type_id_input_ladder, -1)

        # Add url related to this
        self.db.activity_uri_create(activity_db_id, uri, "", -1)

        # Add category of result (bad, bronze, silver, gold)
        self.db.activity_parameter_create(activity_db_id,type_id_param,-1,-1)

        return 1

    def options_location(self, activity_type_name):
        """Function to give options to end user.
        Should repeat unless correct option is chosen.
        """
        # Prepare input data
        set_ids = self.set_type_ids(activity_type_name)
        if not set_ids:
            sys.exit("IDs were not defined!")

        # Set options
        options = ["CERN", "IPNL"]
        answers = [self.type_id_loc_cern,self.type_id_loc_ipnl]

        incorrect_choice = True
        while incorrect_choice:
            print("\nWhat is the location of this activity (Choose a number)?")
            for i_option in range(len(options)):
                print("\n " + str(i_option) + " : " + str(options[i_option]))
            choice = input("\nEnter a number: ")

            passed_integer_control = False
            try:
                int(choice)
                passed_integer_control = True
            except ValueError:
                print("\n****** ERROR: Enter an integer! ******")

            if passed_integer_control:
                for i_choice in range(len(options)):
                    if int(choice) == i_choice:
                        return int(answers[i_choice])

                if incorrect_choice:
                    print("\n****** ERROR: Incorrect choice. Please, choose one in range 0 to "
                          + str(len(options) - 1) + "! ******")

    def options_classification_parameter(self, activity_type_name):
        """Function to give options to end user.
        Should repeat unless correct option is chosen.
        """
        # Prepare input data
        set_ids = self.set_type_ids(activity_type_name)
        if not set_ids:
            sys.exit("IDs were not defined!")

        # Set options
        options = ["Gold", "Silver", "Bronze", "Bad", "Classification not defined - Leave it empty"]
        answers = [self.type_id_param_gold,self.type_id_param_silver,
                   self.type_id_param_bronze,self.type_id_param_bad,-1]

        incorrect_choice = True
        while incorrect_choice:
            print("\nWhat is the classification of this activity (Choose a number)?")
            for i_option in range(len(options)):
                print("\n " + str(i_option) + " : " + str(options[i_option]))
            choice = input("\nEnter a number: ")

            passed_integer_control = False
            try:
                int(choice)
                passed_integer_control = True
            except ValueError:
                print("\n****** ERROR: Enter an integer! ******")

            if passed_integer_control:
                for i_choice in range(len(options)):
                    if int(choice) == i_choice:
                        return int(answers[i_choice])

                if incorrect_choice:
                    print("\n****** ERROR: Incorrect choice. Please, choose one in range 0 to "
                          + str(len(options) - 1) + "! ******")

    def activity_decision_tree(self):
        """Give set a questions and return the correct activity type name.
        """
        input_file = open("config/allowed_activities.txt")
        allowed_activities = input_file.read()
        input_file.close()
        allowed_activities = ast.literal_eval(allowed_activities)

        # Block to let user to choose activity
        incorrect_choice = True
        chosen_activity = ""
        while incorrect_choice:
            print("\nWhat activity do you want?")
            i_activity = 0
            for allowed_activity in allowed_activities:
                print("\n Option " + str(i_activity) + " : " + str(allowed_activity['activity']))
                i_activity += 1
            chosen_activity = str(input("\nYour input: "))

            passed_integer_control = False
            try:
                int(chosen_activity)
                passed_integer_control = True
            except ValueError:
                print("\n****** ERROR: Enter an integer! ******")

            if passed_integer_control:
                if int(chosen_activity) < i_activity:
                    incorrect_choice = False
                else:
                    print("\n****** ERROR: Incorrect choice. Please, choose one in range 0 to "
                          + str(i_activity - 1) + "! ******")

        # Obtaining activity name
        i_activity = 0
        activity_type_name = ""
        for allowed_activity in allowed_activities:
            if i_activity == int(chosen_activity):
                activity_type_name = allowed_activity['activity']
            i_activity += 1

        if not activity_type_name:
            sys.exit("INTERNAL ERROR: Inform roman.lavicka@cern.ch")

        # Block to let user to choose number of chips
        n_chips_ = self.set_n_chips()

        activity_name = str(activity_type_name) + "-" + str(n_chips_)

        return activity_name
